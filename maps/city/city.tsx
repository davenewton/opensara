<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="city" tilewidth="32" tileheight="32" tilecount="70" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1" x="1" y="1" width="256" height="256">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="2" x="258" y="1" width="256" height="256">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="3" x="515" y="1" width="256" height="256">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="19" x="1" y="390" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="20" x="34" y="390" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="21" x="1" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
  <animation>
   <frame tileid="210" duration="100"/>
   <frame tileid="211" duration="100"/>
   <frame tileid="212" duration="100"/>
   <frame tileid="213" duration="100"/>
  </animation>
 </tile>
 <tile id="22" x="34" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
  <animation>
   <frame tileid="220" duration="100"/>
   <frame tileid="221" duration="100"/>
   <frame tileid="222" duration="100"/>
   <frame tileid="223" duration="100"/>
  </animation>
 </tile>
 <tile id="23" x="67" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
  <animation>
   <frame tileid="230" duration="100"/>
   <frame tileid="231" duration="100"/>
   <frame tileid="232" duration="100"/>
   <frame tileid="233" duration="100"/>
  </animation>
 </tile>
 <tile id="24" x="100" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
  <animation>
   <frame tileid="240" duration="100"/>
   <frame tileid="241" duration="100"/>
   <frame tileid="242" duration="100"/>
   <frame tileid="243" duration="100"/>
  </animation>
 </tile>
 <tile id="25" x="133" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
  <animation>
   <frame tileid="250" duration="100"/>
   <frame tileid="251" duration="100"/>
   <frame tileid="252" duration="100"/>
   <frame tileid="253" duration="100"/>
  </animation>
 </tile>
 <tile id="26" x="166" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
  <animation>
   <frame tileid="260" duration="100"/>
   <frame tileid="261" duration="100"/>
   <frame tileid="262" duration="100"/>
   <frame tileid="263" duration="100"/>
  </animation>
 </tile>
 <tile id="27" x="67" y="390" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="28" x="100" y="390" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="29" x="133" y="390" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="30" x="199" y="390" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="31" x="199" y="291" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
  <animation>
   <frame tileid="310" duration="100"/>
   <frame tileid="311" duration="100"/>
   <frame tileid="312" duration="100"/>
  </animation>
 </tile>
 <tile id="32" x="199" y="258" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="33" x="232" y="258" width="32" height="32" type="sudo">
  <properties>
   <property name="letter" value="s"/>
  </properties>
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="34" x="265" y="258" width="32" height="32" type="sudo">
  <properties>
   <property name="letter" value="u"/>
  </properties>
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="35" x="298" y="258" width="32" height="32" type="sudo">
  <properties>
   <property name="letter" value="d"/>
  </properties>
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="36" x="331" y="258" width="32" height="32" type="o">
  <properties>
   <property name="letter" value="o"/>
  </properties>
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="37" x="364" y="258" width="32" height="32" type="root">
  <properties>
   <property name="letter" value="r"/>
  </properties>
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="38" x="331" y="258" width="32" height="32" type="root">
  <properties>
   <property name="letter" value="o"/>
  </properties>
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="39" x="397" y="258" width="32" height="32" type="root">
  <properties>
   <property name="letter" value="t"/>
  </properties>
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="40" x="232" y="390" width="32" height="18">
  <image width="1024" height="1024" source="city.png"/>
  <animation>
   <frame tileid="400" duration="100"/>
   <frame tileid="401" duration="100"/>
   <frame tileid="402" duration="100"/>
   <frame tileid="403" duration="100"/>
  </animation>
 </tile>
 <tile id="41" x="959" y="69" width="64" height="16" type="disappearing-platform">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="42" x="496" y="258" width="32" height="64" type="door">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="210" x="1" y="258" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="211" x="1" y="291" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="212" x="1" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="213" x="1" y="357" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="220" x="34" y="258" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="221" x="34" y="291" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="222" x="34" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="223" x="34" y="357" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="230" x="67" y="258" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="231" x="67" y="291" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="232" x="67" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="233" x="67" y="357" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="240" x="100" y="258" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="241" x="100" y="291" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="242" x="100" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="243" x="100" y="357" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="250" x="133" y="258" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="251" x="133" y="291" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="252" x="133" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="253" x="133" y="357" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="260" x="166" y="258" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="261" x="166" y="291" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="262" x="166" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="263" x="166" y="357" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="310" x="199" y="291" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="311" x="199" y="324" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="312" x="199" y="357" width="32" height="32">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="400" x="232" y="390" width="32" height="18">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="401" x="265" y="390" width="32" height="18">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="402" x="298" y="390" width="32" height="18">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="403" x="331" y="390" width="32" height="18">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1000" x="1" y="426" width="64" height="148">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1001" x="66" y="424" width="32" height="150">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1002" x="99" y="426" width="64" height="147">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1003" x="164" y="449" width="56" height="124">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1004" x="221" y="432" width="64" height="141">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1005" x="285" y="512" width="48" height="61">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1006" x="334" y="431" width="32" height="142">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1007" x="367" y="432" width="38" height="141">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1008" x="406" y="456" width="49" height="117">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1009" x="456" y="434" width="18" height="139">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1010" x="475" y="390" width="25" height="184">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1011" x="501" y="491" width="64" height="82">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
 <tile id="1012" x="566" y="454" width="40" height="119">
  <image width="1024" height="1024" source="city.png"/>
 </tile>
</tileset>
