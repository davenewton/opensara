<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.0" name="sara_home" tilewidth="32" tileheight="32" tilecount="35" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>

<!-- grounds -->
 <tile id="1" x="1" y="577" width="32" height="32">
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
 <tile id="2" x="34" y="577" width="32" height="32">
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
 <tile id="3" x="67" y="577" width="32" height="32">
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>

<!-- house -->
 <tile id="20" x="438" y="577" width="176" height="157">
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>

 <!-- background -->
 <tile id="30" x="0" y="0" width="1024" height="576">
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>

 <!-- kyle -->
 <tile id="40" x="903" y="577" width="47" height="26" type="kyle">
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>

 <!-- clouds -->
 <tile id="50" x="100" y="577" width="92" height="44" >
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
 <tile id="51" x="193" y="577" width="117" height="53" >
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
  <tile id="52" x="311" y="577" width="126" height="46" >
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
  <tile id="53" x="100" y="622" width="98" height="44" >
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
   <tile id="54" x="199" y="630" width="108" height="39" >
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
<tile id="55" x="311" y="625" width="82" height="36" >
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
<tile id="56" x="100" y="667" width="108" height="39" >
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
 <tile id="57" x="209" y="670" width="67" height="29" >
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
 <tile id="58" x="277" y="670" width="70" height="30" >
  <image width="1024" height="1024" source="sara_home.png"/>
 </tile>
 
 </tileset>
