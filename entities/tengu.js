import Enemy from "./enemy.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import Sara from "./sara.js";
import { BulletAnimations, Gun } from "./bullet.js";

/**
 * @readonly
 * @enum {number}
 */
const TenguState = {
    IDLE: 1,
    HURT: 2,
    DYING: 3,
    DEAD: 4
};

/**
 * 
 * @type TenguAnimations
 */
class TenguAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    fly;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    hurt;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    dying;
}

export default class Tengu extends Enemy {

    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * 
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     *  @type TenguState
     */
    state;

    /**
     * @type number
     */
    health = 3;

    get dead() {
        return this.health <= 0;
    }

    /**
     * @type Gun
     */
    gun;

    /**
     * @type number
     */
    flySpeed = 2;

    /**
     * @type number
     */
    fleeSpeed = 4;

    /**
     * @type TenguAnimations
     */
    static animations;

    /**
     * @type BulletAnimations
     */
    static bulletAnimations;

    /**
     * @type Array<Playnewton.PPU_Point>
     */
    destinations = [];

    /**
     * @type number
     */
    currentDestination = 0;

    /**
     * @type number
     */
    destinationIncrement = 1;


    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/tengu.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "fly00", x: 2, y: 2, w: 29, h: 48 },
            { name: "fly01", x: 2, y: 52, w: 29, h: 48 },
            { name: "fly02", x: 34, y: 2, w: 29, h: 48 },
            { name: "hurt00", x: 34, y: 52, w: 29, h: 48 },
            { name: "bullet-fly00", x: 64, y: 1, w: 16, h: 16 },
            { name: "bullet-fly01", x: 64, y: 18, w: 16, h: 16 },
            { name: "bullet-explode00", x: 64, y: 34, w: 16, h: 16 },
            { name: "bullet-explode01", x: 64, y: 51, w: 16, h: 16 },
            { name: "bullet-explode02", x: 64, y: 68, w: 16, h: 16 },
            { name: "bullet-explode03", x: 64, y: 85, w: 16, h: 16 }
        ]);

        Tengu.animations = new TenguAnimations();

        Tengu.animations.fly = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "fly00", delay: 100 },
            { name: "fly01", delay: 100 }
        ]);

        Tengu.animations.dying = Tengu.animations.hurt = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "fly00", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "fly01", delay: 100 },
            { name: "hurt00", delay: 100 }
        ]);

        Tengu.bulletAnimations = new BulletAnimations();

        Tengu.bulletAnimations.grow = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullet-fly00", delay: 100 }
        ]);
        
        Tengu.bulletAnimations.fly = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullet-fly00", delay: 100 },
            { name: "bullet-fly01", delay: 100 }
        ]);

        Tengu.bulletAnimations.explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullet-explode00", delay: 100 },
            { name: "bullet-explode01", delay: 100 },
            { name: "bullet-explode02", delay: 100 },
            { name: "bullet-explode03", delay: 100 }
        ]);
    }

    static Unload() {
        Tengu.animations = null;
        Tengu.bulletAnimations = null;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     * @param {number} xDest 
     * @param {number} yDest 
     */
    constructor(x, y, xDest, yDest) {
        super();
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Tengu.animations.fly);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.ENEMIES);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 0, 0, 12, 42);
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -this.flySpeed, this.flySpeed, -this.flySpeed, this.flySpeed);
        Playnewton.PPU.EnableBody(this.body);

        this.state = TenguState.IDLE;        
        this.gun = new Gun({animations: Tengu.bulletAnimations, fireSound: "sounds/shoot-tengu.wav", maxBullets: 5, maxBulletsPerShot: 1,  delayBetweenTwoShotsInMs: 100, growSpeed: 0.1});
        for(let d=0; d<16; d++) {
            this.destinations.push(new Playnewton.PPU_Point(xDest + Math.sin(2 * d / Math.PI) * this.body.width * 4, yDest + d * this.body.height / 2));
        }
    }

    UpdateSprite() {
        switch (this.state) {
            case TenguState.IDLE:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Tengu.animations.fly);
                break;
            case TenguState.HURT:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Tengu.animations.hurt, Playnewton.GPU_AnimationMode.ONCE);
                if (this.sprite.animationStopped) {
                    Playnewton.GPU.SetSpriteAnimation(this.sprite, Tengu.animations.fly);
                    this.state = TenguState.IDLE;
                }
                break;
            case TenguState.DYING:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Tengu.animations.dying);
                break;
        }
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x - 7, this.body.position.y - 7);
        this.gun.UpdateSprite();
    }

    UpdateBody() {
        switch (this.state) {
            case TenguState.IDLE:
                this.flyAround();
                break;
            case TenguState.HURT:
                this.flyAround();
                break;
            case TenguState.DYING:
                Playnewton.PPU.SetBodyVelocityBounds(this.body, -this.fleeSpeed, this.fleeSpeed, -this.fleeSpeed, this.fleeSpeed);
                Playnewton.PPU.SetBodyVelocity(this.body, -this.fleeSpeed, this.fleeSpeed);
                if (!Playnewton.PPU.CheckIfBodyIsInWorldBound(this.body)) {
                    Playnewton.GPU.DisableSprite(this.sprite);
                    Playnewton.PPU.DisableBody(this.body);
                    this.state = TenguState.DEAD;
                }
                break;
            case TenguState.DEAD:
                break;
        }
        this.gun.UpdateBody();
    }

    flyAround() {
        if(this.destinations.length > 0) {
            let destination = this.destinations[this.currentDestination];
            let dx = destination.x - this.body.centerX;
            let dy = destination.y - this.body.centerY;
            let distance = Math.sqrt(dx ** 2 + dy ** 2);
            if(distance < 32) {
                this.currentDestination += this.destinationIncrement;
                if(this.currentDestination >= this.destinations.length) {
                    this.currentDestination = this.destinations.length - 1;
                    this.destinationIncrement = -1;
                }
                if(this.currentDestination <= 0) {
                    this.currentDestination = 0;
                    this.destinationIncrement = 1;
                }
            }
            let s = this.flySpeed / distance;
            dx *= s;
            dy *= s;
            Playnewton.PPU.SetBodyVelocity(this.body, dx, dy);
        }
    }

    get bulletable() {
        return true;
    }

    /**
     * 
     * @param {Sara} sara 
     */
    Pursue(sara) {
        switch (this.state) {
            case TenguState.IDLE:
                this.gun.fire(this.body.position.x - 18, this.body.position.y + 23, -8, 0);
                break;
        }
        this.gun.Pursue(sara);
    }

    Hurt() {
        switch (this.state) {
            case TenguState.IDLE:
                this.state = TenguState.HURT;
                this.health = Math.max(this.health - 1, 0);
                if (this.dead) {
                    this.state = TenguState.DYING;
                } else {
                    Playnewton.APU.PlaySound("sounds/hurt-sara.wav");
                }
            break;
        }
    }
}