import Enemy from "./enemy.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import { Bullet, BulletAnimations } from "./bullet.js";
import Vulnerable from "./vulnerable.js";
import Sara from "./sara.js";

/**
 * @readonly
 * @enum {number}
 */
const AnnieState = {
    INITIAL: 0,
    HAIL_SARA: 1,
    CROUCH: 2,
    JUMP: 3,
    FALL_TO_ATTACK_POSITION: 4,
    SHOOT: 5,
    TORNADO_START: 6,
    TORNADO_MOVE: 7,
    FALL_TO_CRY_POSITION: 10,    
    HURTED: 20,
    CRY: 30
};

/**
 * 
 * @type AnnieAnimations
 */
class AnnieAnimations {

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    cry;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    hurted;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    tornadoStart;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    tornadoMove;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    talk;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    shootLeft;



    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    shootUpLeft;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    shootDownLeft;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    shootRight;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    shootUpRight;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    shootDownRight;

    /**
      * @type Playnewton.GPU_SpriteAnimation
      */
    crouchLeft;

    /**
      * @type Playnewton.GPU_SpriteAnimation
      */
    crouchRight;

    /**
      * @type Playnewton.GPU_SpriteAnimation
      */
    jumpLeft;

    /**
      * @type Playnewton.GPU_SpriteAnimation
      */
    jumpRight;

    /**
     * @type BulletAnimations
     */
    bulletAttack = new BulletAnimations();
}

export class AnnieAttackPlatform {
    /**
     * @type number
     */
    minX;

    /**
     * @type number
     */
    maxX;
}

export default class Annie extends Enemy {

    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Playnewton.PPU_Body
     */
    body;

    tornadoRoundTripCounter = 0;

    static TORNADO_SPEED = 3;
    static TORNADO_MAX_ROUNDTRIP = 4;

    /**
    *  @type AnnieState
    */
    state;

    /**
     * @type Playnewton.PPU_Point
     */
    vectorToSara = new Playnewton.PPU_Point(-1, 0);

    /**
     * @type number
     */
    maxHealth = 10;

    /**
     * @type number
     */
    health = 5;

    /**
     * @type AnnieAnimations
     */
    static animations;

    /**
     * @type Array<Bullet>
     */
    bullets = [];

    static MAX_BULLETS = 20;
    static BULLET_SPEED = 8;

    /**
     * @type Playnewton.PPU_Point
     */
    hailPosition;

    /**
     * @type Playnewton.PPU_Point
     */
    cryPosition;

    /**
     * @type Array<AnnieAttackPlatform>
     */
    attackPlatforms = [];

    /**
     * @type AnnieAttackPlatform
     */
    currentAttackPlatform;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/annie.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "cry0", x: 1, y: 1, w: 48, h: 55 },
            { name: "cry1", x: 50, y: 1, w: 48, h: 55 },
            { name: "cry2", x: 99, y: 1, w: 48, h: 55 },
            { name: "hurted0", x: 148, y: 1, w: 48, h: 55 },
            { name: "hurted1", x: 197, y: 1, w: 48, h: 55 },
            { name: "hurted2", x: 246, y: 1, w: 48, h: 55 },
            { name: "hurted3", x: 295, y: 1, w: 48, h: 55 },
            { name: "tornadoStart0", x: 344, y: 1, w: 48, h: 55 },
            { name: "tornadoStart1", x: 393, y: 1, w: 48, h: 55 },
            { name: "tornadoStart2", x: 442, y: 1, w: 48, h: 55 },
            { name: "tornadoStart3", x: 1, y: 57, w: 48, h: 55 },
            { name: "tornadoStart4", x: 50, y: 57, w: 48, h: 55 },
            { name: "tornadoStart5", x: 99, y: 57, w: 48, h: 55 },
            { name: "tornadoStart6", x: 148, y: 57, w: 48, h: 55 },
            { name: "tornadoMove0", x: 148, y: 57, w: 48, h: 55 },
            { name: "tornadoMove1", x: 197, y: 57, w: 48, h: 55 },
            { name: "tornadoMove2", x: 246, y: 57, w: 48, h: 55 },
            { name: "tornadoMove3", x: 295, y: 57, w: 48, h: 55 },
            { name: "tornadoMove4", x: 344, y: 57, w: 48, h: 55 },
            { name: "tornadoMove5", x: 393, y: 57, w: 48, h: 55 },
            { name: "talk0", x: 442, y: 57, w: 48, h: 55 },
            { name: "talk1", x: 1, y: 113, w: 48, h: 55 },
            { name: "shootLeft0", x: 50, y: 113, w: 48, h: 55 },
            { name: "shootLeft1", x: 99, y: 113, w: 48, h: 55 },
            { name: "shootLeft2", x: 148, y: 113, w: 48, h: 55 },
            { name: "shootLeft3", x: 197, y: 113, w: 48, h: 55 },
            { name: "shootUpLeft0", x: 246, y: 113, w: 48, h: 55 },
            { name: "shootUpLeft1", x: 295, y: 113, w: 48, h: 55 },
            { name: "shootUpLeft2", x: 344, y: 113, w: 48, h: 55 },
            { name: "shootUpLeft3", x: 393, y: 113, w: 48, h: 55 },
            { name: "shootDownLeft0", x: 442, y: 169, w: 48, h: 55 },
            { name: "shootDownLeft1", x: 1, y: 225, w: 48, h: 55 },
            { name: "shootDownLeft2", x: 50, y: 225, w: 48, h: 55 },
            { name: "shootDownLeft3", x: 99, y: 225, w: 48, h: 55 },
            { name: "shootRight0", x: 442, y: 113, w: 48, h: 55 },
            { name: "shootRight1", x: 1, y: 169, w: 48, h: 55 },
            { name: "shootRight2", x: 50, y: 169, w: 48, h: 55 },
            { name: "shootRight3", x: 99, y: 169, w: 48, h: 55 },
            { name: "shootUpRight0", x: 148, y: 169, w: 48, h: 55 },
            { name: "shootUpRight1", x: 197, y: 169, w: 48, h: 55 },
            { name: "shootUpRight2", x: 246, y: 169, w: 48, h: 55 },
            { name: "shootUpRight3", x: 295, y: 169, w: 48, h: 55 },
            { name: "shootDownRight0", x: 246, y: 225, w: 48, h: 55 },
            { name: "shootDownRight1", x: 295, y: 225, w: 48, h: 55 },
            { name: "shootDownRight2", x: 344, y: 225, w: 48, h: 55 },
            { name: "shootDownRight3", x: 393, y: 225, w: 48, h: 55 },
            { name: "crouchLeft0", x: 344, y: 169, w: 48, h: 55 },
            { name: "crouchRight0", x: 148, y: 225, w: 48, h: 55 },
            { name: "jumpLeft0", x: 393, y: 169, w: 48, h: 55 },
            { name: "jumpRight0", x: 197, y: 225, w: 48, h: 55 },
            { name: "bulletGrow0", x: 1, y: 281, w: 16, h: 16 },
            { name: "bulletGrow1", x: 18, y: 281, w: 16, h: 16 },
            { name: "bulletFly0", x: 35, y: 281, w: 16, h: 16 },
            { name: "bulletFly1", x: 52, y: 281, w: 16, h: 16 },
            { name: "bulletExplode0", x: 69, y: 281, w: 16, h: 16 },
            { name: "bulletExplode1", x: 86, y: 281, w: 16, h: 16 },
            { name: "bulletExplode2", x: 103, y: 281, w: 16, h: 16 },
            { name: "bulletExplode3", x: 120, y: 281, w: 16, h: 16 },
            { name: "bulletExplode4", x: 137, y: 281, w: 16, h: 16 }
        ]);

        Annie.animations = new AnnieAnimations();

        Annie.animations.cry = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "cry0", delay: 500 },
            { name: "cry1", delay: 500 },
            { name: "cry2", delay: 500 }
        ]);

        Annie.animations.hurted = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "hurted0", delay: 50 },
            { name: "hurted1", delay: 50 },
            { name: "hurted2", delay: 50 },
            { name: "hurted3", delay: 50 },
            { name: "hurted0", delay: 50 },
            { name: "hurted1", delay: 50 },
            { name: "hurted2", delay: 50 },
            { name: "hurted3", delay: 50 },
            { name: "hurted0", delay: 50 },
            { name: "hurted1", delay: 50 },
            { name: "hurted2", delay: 50 },
            { name: "hurted3", delay: 50 },
            { name: "hurted0", delay: 50 },
            { name: "hurted1", delay: 50 },
            { name: "hurted2", delay: 50 },
            { name: "hurted3", delay: 50 }
        ]);

        Annie.animations.tornadoStart = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "tornadoStart0", delay: 100 },
            { name: "tornadoStart1", delay: 100 },
            { name: "tornadoStart2", delay: 100 },
            { name: "tornadoStart3", delay: 100 },
            { name: "tornadoStart4", delay: 100 },
            { name: "tornadoStart5", delay: 100 },
            { name: "tornadoStart6", delay: 100 }
        ]);

        Annie.animations.tornadoMove = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "tornadoMove0", delay: 50 },
            { name: "tornadoMove1", delay: 50 },
            { name: "tornadoMove2", delay: 50 },
            { name: "tornadoMove3", delay: 50 },
            { name: "tornadoMove4", delay: 50 },
            { name: "tornadoMove5", delay: 50 }
        ]);

        Annie.animations.talk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "talk0", delay: 1000 },
            { name: "talk1", delay: 100 },
            { name: "talk0", delay: 100 },
            { name: "talk1", delay: 100 }
        ]);

        Annie.animations.shootLeft = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "shootLeft0", delay: 100 },
            { name: "shootLeft1", delay: 100 },
            { name: "shootLeft2", delay: 100 },
            { name: "shootLeft3", delay: 100 }
        ]);

        Annie.animations.shootUpLeft = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "shootUpLeft0", delay: 100 },
            { name: "shootUpLeft1", delay: 100 },
            { name: "shootUpLeft2", delay: 100 },
            { name: "shootUpLeft3", delay: 100 }
        ]);

        Annie.animations.shootDownLeft = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "shootDownLeft0", delay: 100 },
            { name: "shootDownLeft1", delay: 100 },
            { name: "shootDownLeft2", delay: 100 },
            { name: "shootDownLeft3", delay: 100 }
        ]);

        Annie.animations.shootRight = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "shootRight0", delay: 100 },
            { name: "shootRight1", delay: 100 },
            { name: "shootRight2", delay: 100 },
            { name: "shootRight3", delay: 100 }
        ]);

        Annie.animations.shootUpRight = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "shootUpRight0", delay: 100 },
            { name: "shootUpRight1", delay: 100 },
            { name: "shootUpRight2", delay: 100 },
            { name: "shootUpRight3", delay: 100 }
        ]);

        Annie.animations.shootDownRight = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "shootDownRight0", delay: 100 },
            { name: "shootDownRight1", delay: 100 },
            { name: "shootDownRight2", delay: 100 },
            { name: "shootDownRight3", delay: 100 }
        ]);

        Annie.animations.crouchLeft = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "crouchLeft0", delay: 500 },
        ]);

        Annie.animations.crouchRight = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "crouchRight0", delay: 500 },
        ]);

        Annie.animations.jumpLeft = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "jumpLeft0", delay: 1000 },
        ]);

        Annie.animations.jumpRight = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "jumpRight0", delay: 1000 },
        ]);

        Annie.animations.bulletAttack.grow =Annie.animations.bulletAttack.fly = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bulletFly0", delay: 100 },
            { name: "bulletFly1", delay: 100 },
        ]);

        Annie.animations.bulletAttack.explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bulletExplode0", delay: 100 },
            { name: "bulletExplode1", delay: 100 },
            { name: "bulletExplode2", delay: 100 },
            { name: "bulletExplode3", delay: 100 },
            { name: "bulletExplode4", delay: 100 },
        ]);
    }

    static Unload() {
        Annie.animations = null;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        super();
        this.cryPosition = this.hailPosition = new Playnewton.PPU_Point(x, y);

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Annie.animations.talk);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.ENEMIES);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 19, 8, 20, 47);
        Playnewton.PPU.SetBodyPosition(this.body, x, -55);
        Playnewton.PPU.SetBodyCollideWorldBounds(this.body, false);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -10, 10, -20, 10);
        Playnewton.PPU.SetBodyAffectedByGravity(this.body, true);

        this.state = AnnieState.INITIAL;

        for (let i = 0; i < Annie.MAX_BULLETS; ++i) {
            let bullet = new Bullet(Annie.animations.bulletAttack);
            bullet.growSpeed = 1;
            this.bullets.push(bullet);
        }
    }

    UpdateBody() {
        switch (this.state) {
            case AnnieState.CROUCH:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                break;
            case AnnieState.JUMP:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, -32);
                if (this.sprite.y < -this.sprite.height) {
                    if(this.dead) {
                        this.FallAndCry();
                    } else {
                        this.Attack();
                    }
                }
                break;
            case AnnieState.FALL_TO_ATTACK_POSITION:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 16);
                if (this.body.touches.bottom) {
                    if(Math.random() < 0.3) {
                        this.TornadoStart();
                    } else {
                        this.Shoot();
                    }
                }
                break;
            case AnnieState.FALL_TO_CRY_POSITION:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 16);
                if (this.body.touches.bottom) {
                    this.Cry();
                }
                break;
            case AnnieState.TORNADO_MOVE:
                let tornadoVx = this.body.velocity.x;
                if(this.body.left <= this.currentAttackPlatform.minX) {
                    tornadoVx = Annie.TORNADO_SPEED;
                } else if(this.body.right >= this.currentAttackPlatform.maxX) {
                    tornadoVx = -Annie.TORNADO_SPEED;
                } else if(Math.abs(this.body.velocity.x) < Annie.TORNADO_SPEED) {
                    tornadoVx = Annie.TORNADO_SPEED;
                }
                if(tornadoVx !== this.body.velocity.x) {
                    Playnewton.PPU.SetBodyVelocity(this.body, tornadoVx, 0);
                    this.tornadoRoundTripCounter++;
                }
                if(this.tornadoRoundTripCounter > Annie.TORNADO_MAX_ROUNDTRIP && Math.abs(this.body.centerX - (this.currentAttackPlatform.maxX + this.currentAttackPlatform.minX) / 2) < this.body.width) {
                    this.CrouchAndJump();
                }
                break;
            case AnnieState.SHOOT:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                let canShoot;
                switch(this.sprite.animation) {
                    case Annie.animations.shootUpLeft:
                    case Annie.animations.shootDownLeft:
                    case Annie.animations.shootUpRight:
                    case Annie.animations.shootDownRight:
                    case Annie.animations.shootLeft:
                    case Annie.animations.shootRight:
                        canShoot = this.sprite.animationStopped;
                        break;
                    default:
                        canShoot = true;
                }
                if (canShoot) {
                    let bullet = this.bullets.find((bullet) => bullet.canBeFired);
                    if (bullet) {
                        Playnewton.APU.PlaySound("sounds/shoot-annie.wav");
                        if (this.vectorToSara.y < -32) {
                            if (this.vectorToSara.x < 0) {
                                bullet.fire(this.sprite.left + 5, this.sprite.top + 1, Math.sin(-120 * Math.PI / 180.0) * Annie.BULLET_SPEED, Math.cos(-120 * Math.PI / 180.0) * Annie.BULLET_SPEED);
                                Playnewton.GPU.ResetSpriteAnimation(this.sprite, Annie.animations.shootUpLeft, Playnewton.GPU_AnimationMode.ONCE);
                            } else {
                                bullet.fire(this.sprite.left + 42, this.sprite.top + 1, Math.sin(120 * Math.PI / 180.0) * Annie.BULLET_SPEED, Math.cos(120 * Math.PI / 180.0) * Annie.BULLET_SPEED);
                                Playnewton.GPU.ResetSpriteAnimation(this.sprite, Annie.animations.shootUpRight, Playnewton.GPU_AnimationMode.ONCE);
                            }
                        } else if (this.vectorToSara.y > 32) {
                            if (this.vectorToSara.x < 0) {
                                bullet.fire(this.sprite.left, this.sprite.top + 38, Math.sin(-65 * Math.PI / 180.0) * Annie.BULLET_SPEED, Math.cos(-65 * Math.PI / 180.0) * Annie.BULLET_SPEED);
                                Playnewton.GPU.ResetSpriteAnimation(this.sprite, Annie.animations.shootDownLeft, Playnewton.GPU_AnimationMode.ONCE);
                            } else {
                                bullet.fire(this.sprite.left + 47, this.sprite.top + 38, Math.sin(65 * Math.PI / 180.0) * Annie.BULLET_SPEED, Math.cos(65 * Math.PI / 180.0) * Annie.BULLET_SPEED);
                                Playnewton.GPU.ResetSpriteAnimation(this.sprite, Annie.animations.shootDownRight, Playnewton.GPU_AnimationMode.ONCE);
                            }
                        } else if (this.vectorToSara.x < 0) {
                            bullet.fire(this.sprite.left, this.sprite.top + 24, -Annie.BULLET_SPEED, 0);
                            Playnewton.GPU.ResetSpriteAnimation(this.sprite, Annie.animations.shootLeft, Playnewton.GPU_AnimationMode.ONCE);
                        } else {
                            bullet.fire(this.sprite.right, this.sprite.top + 24, Annie.BULLET_SPEED, 0);
                            Playnewton.GPU.ResetSpriteAnimation(this.sprite, Annie.animations.shootRight, Playnewton.GPU_AnimationMode.ONCE);
                        }
                    }
                }
                break;
            case AnnieState.HAIL_SARA:
                break;
            case AnnieState.CRY:
                break;
        }
        this.bullets.forEach(bullet => bullet.UpdateBody());
    }


    UpdateSprite() {
        switch (this.state) {
            case AnnieState.CROUCH:
                if (this.sprite.animationStopped) {
                    this._ChangeState(AnnieState.JUMP);
                }
                break;
            case AnnieState.JUMP:
                break;
            case AnnieState.SHOOT:
                break;
            case AnnieState.TORNADO_START:
                if (this.sprite.animationStopped) {
                    this.TornadoMove();
                }
                break;
            case AnnieState.HAIL_SARA:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Annie.animations.talk);
                break;
            case AnnieState.HURTED:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Annie.animations.hurted, Playnewton.GPU_AnimationMode.ONCE);
                if (this.sprite.animationStopped) {
                    this.CrouchAndJump();
                }
                break;
            case AnnieState.CRY:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Annie.animations.cry);
                break;
        }
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
        this.bullets.forEach(bullet => bullet.UpdateSprite());
    }

    /**
     * 
     * @param {Vulnerable} sara 
     */
    Pursue(sara) {
        if(this.state === AnnieState.TORNADO_MOVE && Playnewton.PPU.CheckIfBodiesIntersects(this.body, sara.body)) {
            sara.Hurt();
        }
        this.bullets.forEach(bullet => bullet.Pursue(sara));
    }

    /**
     * 
     * @param {AnnieState} state 
     */
    _ChangeState(state) {
        this.state = state;
        switch (this.state) {
            case AnnieState.CROUCH:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, this.vectorToSara.x < 0 ? Annie.animations.crouchLeft : Annie.animations.crouchRight, Playnewton.GPU_AnimationMode.ONCE);
                break;
            case AnnieState.JUMP:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, this.vectorToSara.x < 0 ? Annie.animations.jumpLeft : Annie.animations.jumpRight);
                break;
            case AnnieState.HAIL_SARA:
                Playnewton.PPU.EnableBody(this.body);
                break;
            case AnnieState.FALL_TO_ATTACK_POSITION:
            case AnnieState.FALL_TO_CRY_POSITION:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, this.vectorToSara.x < 0 ? Annie.animations.jumpLeft : Annie.animations.jumpRight);
                break;
            case AnnieState.CRY:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Annie.animations.cry);
                break;
            case AnnieState.TORNADO_START:
                this.tornadoRoundTripCounter = 0;
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Annie.animations.tornadoStart, Playnewton.GPU_AnimationMode.ONCE);
                break;
            case AnnieState.TORNADO_MOVE:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Annie.animations.tornadoMove);
                break;
        }
    }


    get stompable() {
        switch (this.state) {
            case AnnieState.SHOOT:
                return true;
            default:
                return false;
        }
    }

    get dead() {
        return this.health <= 0;
    }

    get crying() {
        return this.state === AnnieState.CRY;
    }

    HailSara() {
        this._ChangeState(AnnieState.HAIL_SARA);
    }

    CrouchAndJump() {
        this._ChangeState(AnnieState.CROUCH);
    }

    FallAndCry() {
        Playnewton.PPU.SetBodyPosition(this.body, this.cryPosition.x, -55);
        this._ChangeState(AnnieState.FALL_TO_CRY_POSITION);
    }

    Cry() {
        this._ChangeState(AnnieState.CRY);
    }

    Attack() {
        this.currentAttackPlatform = this.attackPlatforms[Math.floor(Math.random() * this.attackPlatforms.length)];
        let attackPosX = this.currentAttackPlatform.minX + Math.floor(Math.random() * (this.currentAttackPlatform.maxX - this.body.width * 2 - this.currentAttackPlatform.minX));
        Playnewton.PPU.SetBodyPosition(this.body, attackPosX, -55);
        this._ChangeState(AnnieState.FALL_TO_ATTACK_POSITION);
    }

    TornadoStart() {
        this._ChangeState(AnnieState.TORNADO_START);
    }

    TornadoMove() {
        this._ChangeState(AnnieState.TORNADO_MOVE);
    }

    Shoot() {
        this._ChangeState(AnnieState.SHOOT);
    }

    Hurt() {
        switch (this.state) {
            case AnnieState.SHOOT:
                this.health = Math.max(this.health - 1, 0);
                this._ChangeState(AnnieState.HURTED);
                break;
        }
    }

    /**
     * @param {Sara} sara 
     */
    LookAtSara(sara) {
        this.vectorToSara.x = sara.body.centerX - this.body.centerX;
        this.vectorToSara.y = sara.body.centerY - this.body.centerY;
    }

}