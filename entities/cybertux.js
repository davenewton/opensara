import Enemy from "./enemy.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import { Gun, BulletAnimations } from "./bullet.js";
import Sara from "./sara.js";
import ExplosionArea from "./explosion_area.js";

/**
 * @readonly
 * @enum {number}
 */
const CybertuxState = {
    INITIAL: 0,
    BLINK: 1,
    TRIPLE_SHOOT: 2,
    SIDE_SHOOT: 3,
    LAUNCH_MISSILE: 4,
    HURTED: 5,
    TELEPORT_MINIS: 6,
    EXPLODE: 7
};

class CybertuxAnimations {

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    idle;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    hurted;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    eyeBlink;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    mouthOpen;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    explode;

    /**
     * @type BulletAnimations
     */
    tripleShootBullets;

    /**
     * @type BulletAnimations
     */
    sideShootBullets;
}

export default class Cybertux extends Enemy {

    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    eyeSprite;

    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    mouthOpenSprite;

    /**
     * @type Playnewton.PPU_Body
     */
    body;

    /**
    *  @type CybertuxState
    */
    state;

    /**
     * @type number
     */
    maxHealth = 10;

    /**
     * @type number
     */
    health = 5;

    /**
     * @type CybertuxAnimations
     */
    static animations;

    /**
     * @type Playnewton.PPU_Point
     */
    initialPosition;

    /**
     * @type Gun
     */
    tripleShootGun;

    /**
     * @type Gun
     */
    sideShootGun;

    /**
     * @type Array<CybertuxMini>
     */
    minis = [];

    /**
     * @type MissileLauncher
     */
    missileLauncher;

    /**
     * @type ExplosionArea
     */
    explosions;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/city/cybertux.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "idle", x: 1, y: 1, w: 640, h: 506 },
            { name: "hurted", x: 1, y: 508, w: 640, h: 506 },
            { name: "eye-blink", x: 819, y: 1, w: 162, h: 77 },
            { name: "mouth-open0", x: 642, y: 1, w: 176, h: 103 },
            { name: "mouth-open1", x: 642, y: 105, w: 176, h: 103 },
            { name: "mouth-open2", x: 642, y: 209, w: 176, h: 103 },
            { name: "mouth-open3", x: 642, y: 313, w: 176, h: 103 },
            { name: "mouth-open4", x: 642, y: 417, w: 176, h: 103 },
            { name: "mouth-open5", x: 642, y: 521, w: 176, h: 103 },
            { name: "tripleshoot0", x: 642, y: 625, w: 32, h: 32 },
            { name: "tripleshoot1", x: 675, y: 625, w: 32, h: 32 },
            { name: "tripleshoot2", x: 708, y: 625, w: 32, h: 32 },
            { name: "tripleshoot3", x: 741, y: 625, w: 32, h: 32 },
            { name: "sideshoot0", x: 774, y: 625, w: 32, h: 32 },
            { name: "sideshoot1", x: 807, y: 625, w: 32, h: 32 },
            { name: "explode0", x: 859, y: 826, w: 32, h: 32 },
            { name: "explode1", x: 892, y: 826, w: 32, h: 32 },
            { name: "explode2", x: 925, y: 826, w: 32, h: 32 },
            { name: "explode3", x: 958, y: 826, w: 32, h: 32 },
            { name: "explode4", x: 991, y: 826, w: 32, h: 32 }

        ]);

        Cybertux.animations = new CybertuxAnimations();

        Cybertux.animations.idle = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "idle", delay: 1000 }
        ]);

        Cybertux.animations.hurted = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "idle", delay: 100 },
            { name: "hurted", delay: 100 }
        ]);

        Cybertux.animations.eyeBlink = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "eye-blink", delay: 500 }
        ]);

        Cybertux.animations.mouthOpen = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "mouth-open0", delay: 150 },
            { name: "mouth-open1", delay: 150 },
            { name: "mouth-open2", delay: 150 },
            { name: "mouth-open3", delay: 150 },
            { name: "mouth-open4", delay: 150 },
            { name: "mouth-open5", delay: 150 }
        ]);

        Cybertux.animations.tripleShootBullets = new BulletAnimations();
        Cybertux.animations.tripleShootBullets.grow = Cybertux.animations.tripleShootBullets.fly = Cybertux.animations.tripleShootBullets.explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "tripleshoot0", delay: 100 },
            { name: "tripleshoot1", delay: 100 },
            { name: "tripleshoot2", delay: 100 },
            { name: "tripleshoot3", delay: 100 }
        ]);

        Cybertux.animations.sideShootBullets = new BulletAnimations();
        Cybertux.animations.sideShootBullets.grow = Cybertux.animations.sideShootBullets.fly = Cybertux.animations.sideShootBullets.explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "sideshoot0", delay: 100 },
            { name: "sideshoot1", delay: 100 }
        ]);

        Cybertux.animations.explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "explode0", delay: 100 },
            { name: "explode1", delay: 100 },
            { name: "explode2", delay: 100 },
            { name: "explode3", delay: 100 },
            { name: "explode4", delay: 100 }
        ]);

        await CybertuxMini.Load();
        await MissileLauncher.Load();
    }

    static Unload() {
        Cybertux.animations = null;

        CybertuxMini.Unload();
        MissileLauncher.Unload();
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        super();

        this.initialPosition = new Playnewton.PPU_Point(x, y - Cybertux.animations.idle.maxHeight);

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.CYBERTUX);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.eyeSprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.eyeSprite, Z_ORDER.CYBERTUX_EYES_AND_MOUTH);

        this.mouthOpenSprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.mouthOpenSprite, Z_ORDER.CYBERTUX_EYES_AND_MOUTH);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 262, 20, 114, 32);
        Playnewton.PPU.SetBodyCollideWorldBounds(this.body, false);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -10, 10, -20, 10);
        Playnewton.PPU.SetBodyAffectedByGravity(this.body, false);

        this.tripleShootGun = new Gun({ animations: Cybertux.animations.tripleShootBullets, fireSound: "sounds/tripleshoot-cybertux.wav", maxBullets: 3, maxBulletsPerShot: 3, delayBetweenTwoShotsInMs: 0, growSpeed: 1 });
        this.sideShootGun = new Gun({ animations: Cybertux.animations.sideShootBullets, fireSound: "sounds/sideshoot-cybertux.wav", maxBullets: 6, maxBulletsPerShot: 1, delayBetweenTwoShotsInMs: 1000, growSpeed: 1 });

        this.missileLauncher = new MissileLauncher({ nbMissiles: 1, nbSights: 1 });

        this._ChangeState(CybertuxState.INITIAL);
    }

    UpdateBody() {
        switch (this.state) {
            case CybertuxState.INITIAL:
                break;
            case CybertuxState.SIDE_SHOOT:
                if (this.body.isGoingDown && this.body.top > (Playnewton.PPU.world.bounds.centerY + 64)) {
                    Playnewton.PPU.SetBodyVelocity(this.body, 0, -1);
                } else if (this.body.isGoingUp && this.body.position.y <= this.initialPosition.y) {
                    this._ChangeState(CybertuxState.INITIAL);
                } else if (this.mouthOpenSprite.animationStopped) {
                    this.sideShootGun.fire(this.mouthOpenSprite.centerX, this.mouthOpenSprite.centerY, Playnewton.RAND.pickOne([-4, 4]), 0);
                }
                break;
            case CybertuxState.HURTED:
                if (this.body.position.y <= this.initialPosition.y) {
                    this._ChangeState(CybertuxState.INITIAL);
                }
                break;
            case CybertuxState.EXPLODE:
                if (this.body.position.y <= this.initialPosition.y) {
                    Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                }
                break;
        }
        this.sideShootGun.UpdateBody();
        this.tripleShootGun.UpdateBody();
        for (let mini of this.minis) {
            mini.UpdateBody();
        }
    }


    UpdateSprite() {
        switch (this.state) {
            case CybertuxState.BLINK:
                if (this.eyeSprite.animationStopped) {
                    this._ChangeState(CybertuxState.INITIAL);
                }
                break;
            case CybertuxState.SIDE_SHOOT:
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                Playnewton.GPU.SetSpritePosition(this.mouthOpenSprite, this.sprite.x + 225, this.sprite.y + 139);
                break;
            case CybertuxState.HURTED:
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                break;
            case CybertuxState.TELEPORT_MINIS:
                for (let mini of this.minis) {
                    mini.Teleport();
                }
                this._ChangeState(CybertuxState.BLINK);
                break;
            case CybertuxState.LAUNCH_MISSILE:
                this.missileLauncher.Aim();
                this._ChangeState(CybertuxState.BLINK);
                break;
            case CybertuxState.EXPLODE:
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                this.explosions.Update();
        }
        this.sideShootGun.UpdateSprite();
        this.tripleShootGun.UpdateSprite();
        for (let mini of this.minis) {
            mini.UpdateSprite();
        }
    }

    get nbInactiveMinis() {
        let n = 0;
        for (let mini of this.minis) {
            if (!mini.active) {
                n++;
            }
        }
        return n;
    }

    /**
     * 
     * @param {Sara} sara 
     */
    Pursue(sara) {
        switch (this.state) {
            case CybertuxState.INITIAL:
                if (this.sprite.animationStopped) {
                    let states = [CybertuxState.TRIPLE_SHOOT, CybertuxState.TRIPLE_SHOOT, CybertuxState.SIDE_SHOOT, CybertuxState.LAUNCH_MISSILE];
                    for (let n = this.nbInactiveMinis; n > 0; n--) {
                        states.push(CybertuxState.TELEPORT_MINIS);
                    }
                    this._ChangeState(Playnewton.RAND.pickOne(states));
                }
                break;
            case CybertuxState.TRIPLE_SHOOT:
                if (this.mouthOpenSprite.animationStopped) {
                    let angle = Math.atan2(sara.body.centerY - this.mouthOpenSprite.centerY, sara.body.centerX - this.mouthOpenSprite.centerX);
                    this.tripleShootGun.fire(this.mouthOpenSprite.centerX, this.mouthOpenSprite.centerY, Math.cos(angle) * 2, Math.sin(angle) * 3);
                    this.tripleShootGun.fire(this.mouthOpenSprite.centerX, this.mouthOpenSprite.centerY, Math.cos(angle + Math.PI / 4) * 3, Math.sin(angle + Math.PI / 4) * 3);
                    this.tripleShootGun.fire(this.mouthOpenSprite.centerX, this.mouthOpenSprite.centerY, Math.cos(angle - Math.PI / 4) * 3, Math.sin(angle - Math.PI / 4) * 3);
                    this._ChangeState(CybertuxState.INITIAL);
                }
                break;
        }
        this.sideShootGun.Pursue(sara);
        this.tripleShootGun.Pursue(sara);
        this.missileLauncher.Pursue(sara);
        for (let mini of this.minis) {
            mini.Pursue(sara);
        }
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    AddMini(x, y) {
        this.minis.push(new CybertuxMini(x, y));
    }

    /**
     * 
     * @param {CybertuxState} state 
     */
    _ChangeState(state) {
        this.state = state;
        switch (this.state) {
            case CybertuxState.INITIAL:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                Playnewton.PPU.SetBodyPosition(this.body, this.initialPosition.x, this.initialPosition.y);

                Playnewton.GPU.ResetSpriteAnimation(this.sprite, Cybertux.animations.idle, Playnewton.GPU_AnimationMode.ONCE);
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);

                Playnewton.GPU.DisableSprite(this.eyeSprite);
                Playnewton.GPU.DisableSprite(this.mouthOpenSprite);
                break;
            case CybertuxState.BLINK:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                Playnewton.PPU.SetBodyPosition(this.body, this.initialPosition.x, this.initialPosition.y);

                Playnewton.GPU.SetSpriteAnimation(this.sprite, Cybertux.animations.idle);
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);

                Playnewton.GPU.SetSpriteAnimation(this.eyeSprite, Cybertux.animations.eyeBlink);
                Playnewton.GPU.ResetSpriteAnimation(this.eyeSprite, Cybertux.animations.eyeBlink, Playnewton.GPU_AnimationMode.ONCE);
                Playnewton.GPU.SetSpritePosition(this.eyeSprite, this.sprite.x + 230, this.sprite.y + 84);
                Playnewton.GPU.EnableSprite(this.eyeSprite);
                break;
            case CybertuxState.TRIPLE_SHOOT:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                Playnewton.PPU.SetBodyPosition(this.body, this.initialPosition.x, this.initialPosition.y);

                Playnewton.GPU.ResetSpriteAnimation(this.sprite, Cybertux.animations.idle, Playnewton.GPU_AnimationMode.ONCE);
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);

                Playnewton.GPU.DisableSprite(this.eyeSprite);

                Playnewton.GPU.SetSpriteAnimation(this.mouthOpenSprite, Cybertux.animations.eyeBlink);
                Playnewton.GPU.ResetSpriteAnimation(this.mouthOpenSprite, Cybertux.animations.mouthOpen, Playnewton.GPU_AnimationMode.ONCE);
                Playnewton.GPU.SetSpritePosition(this.mouthOpenSprite, this.sprite.x + 225, this.sprite.y + 139);
                Playnewton.GPU.EnableSprite(this.mouthOpenSprite);
                break;
            case CybertuxState.SIDE_SHOOT:
                Playnewton.PPU.EnableBody(this.body);
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 1);
                Playnewton.PPU.SetBodyPosition(this.body, this.initialPosition.x, this.initialPosition.y);

                Playnewton.GPU.ResetSpriteAnimation(this.sprite, Cybertux.animations.idle, Playnewton.GPU_AnimationMode.ONCE);
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);

                Playnewton.GPU.DisableSprite(this.eyeSprite);

                Playnewton.GPU.SetSpriteAnimation(this.mouthOpenSprite, Cybertux.animations.eyeBlink);
                Playnewton.GPU.ResetSpriteAnimation(this.mouthOpenSprite, Cybertux.animations.mouthOpen, Playnewton.GPU_AnimationMode.ONCE);
                Playnewton.GPU.SetSpritePosition(this.mouthOpenSprite, this.sprite.x + 225, this.sprite.y + 139);
                Playnewton.GPU.EnableSprite(this.mouthOpenSprite);
                break;
            case CybertuxState.HURTED:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, -1);
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Cybertux.animations.hurted);
                Playnewton.GPU.DisableSprite(this.eyeSprite);
                Playnewton.GPU.DisableSprite(this.mouthOpenSprite);
                break;
            case CybertuxState.EXPLODE:
                for (let mini of this.minis) {
                    mini.Disable();
                }
                Playnewton.PPU.SetBodyVelocity(this.body, 0, -1);
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Cybertux.animations.hurted);
                Playnewton.GPU.DisableSprite(this.eyeSprite);
                Playnewton.GPU.DisableSprite(this.mouthOpenSprite);
                let area = new Playnewton.PPU_Rectangle();
                area.x = this.body.left;
                area.y = 0;
                area.w = this.body.width;
                area.h = Playnewton.GPU.screenHeight;
                this.explosions = new ExplosionArea(area, Cybertux.animations.explode);
                break
        }
    }


    get stompable() {
        switch (this.state) {
            case CybertuxState.SIDE_SHOOT:
                return true;
            default:
                return false;
        }
    }

    HurtByStomp() {
        switch (this.state) {
            case CybertuxState.SIDE_SHOOT:
                this.health = Playnewton.FPU.bound(0, this.health - 1, this.maxHealth);
                this._ChangeState(this.dead ? CybertuxState.EXPLODE : CybertuxState.HURTED);
                break;
        }
    }

    get dead() {
        return this.health <= 0;
    }
}

class CybertuxMiniAnimations {

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    teleport;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    idle;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    shoot;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    hurted;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    walk;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    jump;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    crouch;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    roll;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    aboutToExplode;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    explode;
}

/**
 * @readonly
 * @enum {number}
 */
const CybertuxDirection = {
    LEFT: 0,
    RIGHT: 1
};

/**
 * @readonly
 * @enum {number}
 */
const CybertuxMiniState = {
    INACTIVE: 0,
    TELEPORT: 1,
    IDLE: 2,
    HURTED: 3,
    JUMP: 4,
    CROUCH: 5,
    ROLL: 6,
    ABOUT_TO_EXPLODE: 7,
    EXPLODE: 8,
    SHOOT: 9
};

class CybertuxMini extends Enemy {

    /**
     * @type Playnewton.PPU_Point
     */
    initialPosition;

    /**
     * @type CybertuxMiniState
     */
    state;

    /**
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     * @type Gun
     */
    gun;

    /**
     * @type Playnewton.CLOCK_Timer
     */
    idleTimer;

    /**
     * @type Playnewton.CLOCK_Timer
     */
    explodeTimer;


    /**
     * @type Array<CybertuxMiniAnimations>
     */
    static animations;

    /**
     * @type BulletAnimations
     */
    static bulletAnimations;

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        super();
        this.initialPosition = new Playnewton.PPU_Point(x, y - CybertuxMini.animations[CybertuxDirection.LEFT].idle.maxHeight);
        this.state = CybertuxMiniState.INACTIVE;

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.ENEMIES);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 9, 2, 14, 30);
        Playnewton.PPU.SetBodyPosition(this.body, this.initialPosition.x, this.initialPosition.y);
        Playnewton.PPU.SetBodyCollideWorldBounds(this.body, true);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -10, 10, -20, 10);
        Playnewton.PPU.SetBodyAffectedByGravity(this.body, false);

        this.gun = new Gun({ animations: CybertuxMini.bulletAnimations, fireSound: "sounds/shoot-cybertuxmini.wav", maxBullets: 1, maxBulletsPerShot: 1, delayBetweenTwoShotsInMs: 1000, growSpeed: 1 });
    }

    Teleport() {
        if (this.state === CybertuxMiniState.INACTIVE) {
            this._ChangeState(CybertuxMiniState.TELEPORT);
        }
    }

    Disable() {
        this._ChangeState(CybertuxMiniState.INACTIVE);
    }

    /**
     * @type boolean
     */
    get active() {
        return this.state !== CybertuxMiniState.INACTIVE;
    }

    /**
     * @param {Sara} sara 
     */
    Pursue(sara) {
        switch (this.state) {
            case CybertuxMiniState.ROLL:
                if (sara.isOnGround && Playnewton.PPU.CheckIfBodyRunIntoOther(this.body, sara.body)) {
                    sara.HurtByEnemy();
                }
                break;
            case CybertuxMiniState.EXPLODE:
                if (Playnewton.PPU.CheckIfBodiesIntersects(this.body, sara.body)) {
                    sara.HurtByEnemy();
                }
                break;
            case CybertuxMiniState.SHOOT:
                if (this.sprite.animationStopped) {
                    this.gun.fireAt(this.body.centerX, this.body.top, sara.body.centerX, sara.body.centerY, 4);
                    this._ChangeState(CybertuxMiniState.IDLE);
                }
                break;

        }
        this.gun.Pursue(sara);
    }

    /**
     * @param {CybertuxMiniState} newState 
     */
    _ChangeState(newState) {
        this.state = newState;
        switch (this.state) {
            case CybertuxMiniState.TELEPORT:
                Playnewton.PPU.EnableBody(this.body);
                Playnewton.PPU.SetBodyPosition(this.body, this.initialPosition.x, this.initialPosition.y);
                Playnewton.PPU.SetBodyAffectedByGravity(this.body, false);
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);

                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                Playnewton.GPU.EnableSprite(this.sprite);
                Playnewton.GPU.ResetSpriteAnimation(this.sprite, CybertuxMini.animations[this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].teleport, Playnewton.GPU_AnimationMode.ONCE);
                break;
            case CybertuxMiniState.IDLE:
                this.idleTimer = new Playnewton.CLOCK_Timer(Math.random() * 10000);
                this.idleTimer.Start();
                Playnewton.PPU.SetBodyAffectedByGravity(this.body, true);
                Playnewton.GPU.SetSpriteAnimation(this.sprite, CybertuxMini.animations[this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].idle);
                break;
            case CybertuxMiniState.INACTIVE:
                Playnewton.PPU.DisableBody(this.body);
                Playnewton.GPU.DisableSprite(this.sprite);
                break;
            case CybertuxMiniState.HURTED:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                Playnewton.GPU.ResetSpriteAnimation(this.sprite, CybertuxMini.animations[this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].hurted, Playnewton.GPU_AnimationMode.ONCE);
                break;
            case CybertuxMiniState.JUMP:
                Playnewton.PPU.SetBodyVelocity(this.body, this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? 4 : -4, -20);
                Playnewton.GPU.SetSpriteAnimation(this.sprite, CybertuxMini.animations[this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].jump);
                break;
            case CybertuxMiniState.CROUCH:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                Playnewton.GPU.ResetSpriteAnimation(this.sprite, CybertuxMini.animations[this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].crouch, Playnewton.GPU_AnimationMode.ONCE);
                break;
            case CybertuxMiniState.ROLL:
                this.explodeTimer = new Playnewton.CLOCK_Timer(5000 + Math.random() * 5000);
                this.explodeTimer.Start();
                Playnewton.PPU.SetBodyVelocity(this.body, this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? 4 : -4, 0);
                Playnewton.GPU.SetSpriteAnimation(this.sprite, CybertuxMini.animations[this.body.isGoingRight ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].roll);
                break;
            case CybertuxMiniState.ABOUT_TO_EXPLODE:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                Playnewton.GPU.ResetSpriteAnimation(this.sprite, CybertuxMini.animations[this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].aboutToExplode, Playnewton.GPU_AnimationMode.ONCE);
                break;
            case CybertuxMiniState.EXPLODE:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                Playnewton.PPU.SetBodyAffectedByGravity(this.body, false);
                Playnewton.GPU.ResetSpriteAnimation(this.sprite, CybertuxMini.animations[this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].explode, Playnewton.GPU_AnimationMode.ONCE);
                break;
            case CybertuxMiniState.SHOOT:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                Playnewton.GPU.ResetSpriteAnimation(this.sprite, CybertuxMini.animations[this.initialPosition.x < Playnewton.PPU.world.bounds.centerX ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].shoot, Playnewton.GPU_AnimationMode.ONCE);
                break;
        }
    }

    UpdateBody() {
        switch (this.state) {
            case CybertuxMiniState.IDLE:
                if (this.idleTimer.elapsed) {
                    this._ChangeState(CybertuxMiniState.SHOOT);
                }
                break;
            case CybertuxMiniState.JUMP:
                if (this.body.bottom >= Playnewton.PPU.world.bounds.bottom) {
                    this._ChangeState(CybertuxMiniState.CROUCH);
                }
                break;
            case CybertuxMiniState.ROLL:
                if ((this.body.right >= Playnewton.PPU.world.bounds.right || this.body.touches.right) && this.body.isGoingRight) {
                    Playnewton.PPU.SetBodyVelocity(this.body, -4, 0);
                } else if ((this.body.left <= Playnewton.PPU.world.bounds.left || this.body.touches.left) && this.body.isGoingLeft) {
                    Playnewton.PPU.SetBodyVelocity(this.body, 4, 0);
                }
                if (this.explodeTimer.elapsed) {
                    this._ChangeState(CybertuxMiniState.ABOUT_TO_EXPLODE);
                }
                break;
        }
        this.gun.UpdateBody();
    }

    UpdateSprite() {
        switch (this.state) {
            case CybertuxMiniState.TELEPORT:
                if (this.sprite.animationStopped) {
                    this._ChangeState(CybertuxMiniState.IDLE);
                }
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                break;
            case CybertuxMiniState.IDLE:
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                break;
            case CybertuxMiniState.HURTED:
                if (this.sprite.animationStopped) {
                    this._ChangeState(CybertuxMiniState.JUMP);
                }
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                break;
            case CybertuxMiniState.JUMP:
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                break;
            case CybertuxMiniState.CROUCH:
                if (this.sprite.animationStopped) {
                    this._ChangeState(CybertuxMiniState.ROLL);
                }
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                break;
            case CybertuxMiniState.ROLL:
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                Playnewton.GPU.SetSpriteAnimation(this.sprite, CybertuxMini.animations[this.body.isGoingRight ? CybertuxDirection.RIGHT : CybertuxDirection.LEFT].roll);
                break;
            case CybertuxMiniState.ABOUT_TO_EXPLODE:
                if (this.sprite.animationStopped) {
                    this._ChangeState(CybertuxMiniState.EXPLODE);
                }
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                break;
            case CybertuxMiniState.EXPLODE:
                if (this.sprite.animationStopped) {
                    this._ChangeState(CybertuxMiniState.INACTIVE);
                }
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                break;
            case CybertuxMiniState.SHOOT:
                Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x, this.body.position.y);
                break;
        }
        this.gun.UpdateSprite();
    }

    get stompable() {
        switch (this.state) {
            case CybertuxMiniState.ROLL:
            case CybertuxMiniState.IDLE:
            case CybertuxMiniState.SHOOT:
                return true;
            default:
                return false;
        }
    }

    HurtByStomp() {
        switch (this.state) {
            case CybertuxMiniState.CROUCH:
            case CybertuxMiniState.IDLE:
            case CybertuxMiniState.SHOOT:
                this._ChangeState(CybertuxMiniState.HURTED);
                break;
            case CybertuxMiniState.ROLL:
                this._ChangeState(CybertuxMiniState.ABOUT_TO_EXPLODE);
                break;
        }

    }

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/city/cybertux.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "walk-left0", x: 819, y: 211, w: 32, h: 32 },
            { name: "walk-left1", x: 819, y: 244, w: 32, h: 32 },
            { name: "walk-left2", x: 819, y: 277, w: 32, h: 32 },
            { name: "walk-left3", x: 819, y: 310, w: 32, h: 32 },

            { name: "walk-right0", x: 819, y: 79, w: 32, h: 32 },
            { name: "walk-right1", x: 819, y: 112, w: 32, h: 32 },
            { name: "walk-right2", x: 819, y: 145, w: 32, h: 32 },
            { name: "walk-right3", x: 819, y: 178, w: 32, h: 32 },

            { name: "jump-left0", x: 819, y: 574, w: 32, h: 32 },
            { name: "jump-left1", x: 852, y: 79, w: 32, h: 32 },
            { name: "jump-left2", x: 852, y: 112, w: 32, h: 32 },
            { name: "jump-left3", x: 852, y: 145, w: 32, h: 32 },
            { name: "jump-left4", x: 852, y: 178, w: 32, h: 32 },
            { name: "jump-left5", x: 852, y: 211, w: 32, h: 32 },
            { name: "jump-left6", x: 852, y: 244, w: 32, h: 32 },

            { name: "jump-right0", x: 819, y: 343, w: 32, h: 32 },
            { name: "jump-right1", x: 819, y: 376, w: 32, h: 32 },
            { name: "jump-right2", x: 819, y: 409, w: 32, h: 32 },
            { name: "jump-right3", x: 819, y: 442, w: 32, h: 32 },
            { name: "jump-right4", x: 819, y: 475, w: 32, h: 32 },
            { name: "jump-right5", x: 819, y: 508, w: 32, h: 32 },
            { name: "jump-right6", x: 819, y: 541, w: 32, h: 32 },

            { name: "crouch-left0", x: 852, y: 343, w: 32, h: 32 },
            { name: "crouch-left1", x: 852, y: 376, w: 32, h: 32 },

            { name: "crouch-right0", x: 852, y: 277, w: 32, h: 32 },
            { name: "crouch-right1", x: 852, y: 310, w: 32, h: 32 },

            { name: "roll-left0", x: 852, y: 541, w: 32, h: 32 },
            { name: "roll-left1", x: 852, y: 574, w: 32, h: 32 },
            { name: "roll-left2", x: 885, y: 79, w: 32, h: 32 },
            { name: "roll-left3", x: 885, y: 112, w: 32, h: 32 },

            { name: "roll-right0", x: 852, y: 409, w: 32, h: 32 },
            { name: "roll-right1", x: 852, y: 442, w: 32, h: 32 },
            { name: "roll-right2", x: 852, y: 475, w: 32, h: 32 },
            { name: "roll-right3", x: 852, y: 508, w: 32, h: 32 },

            { name: "teleport0", x: 885, y: 145, w: 32, h: 32 },
            { name: "teleport1", x: 885, y: 178, w: 32, h: 32 },
            { name: "teleport2", x: 885, y: 211, w: 32, h: 32 },

            { name: "teleport-left3", x: 885, y: 244, w: 32, h: 32 },
            { name: "teleport-left4", x: 885, y: 277, w: 32, h: 32 },

            { name: "teleport-right3", x: 885, y: 310, w: 32, h: 32 },
            { name: "teleport-right4", x: 885, y: 343, w: 32, h: 32 },

            { name: "shoot-left", x: 885, y: 409, w: 32, h: 32 },
            { name: "shoot-right", x: 885, y: 376, w: 32, h: 32 },

            { name: "about-to-explode0", x: 885, y: 442, w: 32, h: 32 },
            { name: "about-to-explode1", x: 885, y: 475, w: 32, h: 32 },

            { name: "explode0", x: 885, y: 508, w: 32, h: 32 },
            { name: "explode1", x: 885, y: 541, w: 32, h: 32 },
            { name: "explode2", x: 885, y: 574, w: 32, h: 32 },
            { name: "explode3", x: 918, y: 79, w: 32, h: 32 },
            { name: "explode4", x: 918, y: 112, w: 32, h: 32 },

            { name: "hurted-left0", x: 918, y: 211, w: 32, h: 32 },
            { name: "hurted-left1", x: 918, y: 244, w: 32, h: 32 },
            { name: "hurted-right0", x: 918, y: 145, w: 32, h: 32 },
            { name: "hurted-right1", x: 918, y: 178, w: 32, h: 32 },

            { name: "bullet0", x: 840, y: 624, w: 16, h: 16 },
            { name: "bullet1", x: 840, y: 641, w: 16, h: 16 },
        ]);

        CybertuxMini.animations = [];
        CybertuxMini.animations[CybertuxDirection.LEFT] = new CybertuxMiniAnimations();
        CybertuxMini.animations[CybertuxDirection.RIGHT] = new CybertuxMiniAnimations();


        CybertuxMini.animations[CybertuxDirection.LEFT].idle = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-left1", delay: 1000 },
            { name: "walk-left3", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.LEFT].walk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-left0", delay: 100 },
            { name: "walk-left1", delay: 100 },
            { name: "walk-left2", delay: 100 },
            { name: "walk-left3", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.LEFT].jump = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "jump-left0", delay: 100 },
            { name: "jump-left1", delay: 100 },
            { name: "jump-left2", delay: 100 },
            { name: "jump-left3", delay: 100 },
            { name: "jump-left4", delay: 100 },
            { name: "jump-left5", delay: 100 },
            { name: "jump-left6", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.LEFT].crouch = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "crouch-left0", delay: 100 },
            { name: "crouch-left1", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.LEFT].roll = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "roll-left0", delay: 100 },
            { name: "roll-left1", delay: 100 },
            { name: "roll-left2", delay: 100 },
            { name: "roll-left3", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.LEFT].teleport = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "teleport0", delay: 100 },
            { name: "teleport1", delay: 100 },
            { name: "teleport2", delay: 100 },
            { name: "teleport-left3", delay: 100 },
            { name: "teleport-left4", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.LEFT].shoot = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "shoot-left", delay: 500 }
        ]);

        CybertuxMini.animations[CybertuxDirection.LEFT].hurted = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "hurted-left0", delay: 100 },
            { name: "hurted-left1", delay: 100 },
            { name: "hurted-left0", delay: 100 },
            { name: "hurted-left1", delay: 100 },
            { name: "hurted-left0", delay: 100 },
            { name: "hurted-left1", delay: 100 },
            { name: "hurted-left0", delay: 100 },
            { name: "hurted-left1", delay: 100 },
            { name: "hurted-left0", delay: 100 },
            { name: "hurted-left1", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.RIGHT].idle = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-right1", delay: 1000 },
            { name: "walk-right3", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.RIGHT].walk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "walk-right0", delay: 100 },
            { name: "walk-right1", delay: 100 },
            { name: "walk-right2", delay: 100 },
            { name: "walk-right3", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.RIGHT].jump = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "jump-right0", delay: 100 },
            { name: "jump-right1", delay: 100 },
            { name: "jump-right2", delay: 100 },
            { name: "jump-right3", delay: 100 },
            { name: "jump-right4", delay: 100 },
            { name: "jump-right5", delay: 100 },
            { name: "jump-right6", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.RIGHT].crouch = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "crouch-right0", delay: 100 },
            { name: "crouch-right1", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.RIGHT].roll = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "roll-right0", delay: 100 },
            { name: "roll-right1", delay: 100 },
            { name: "roll-right2", delay: 100 },
            { name: "roll-right3", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.RIGHT].teleport = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "teleport0", delay: 100 },
            { name: "teleport1", delay: 100 },
            { name: "teleport2", delay: 100 },
            { name: "teleport-right3", delay: 100 },
            { name: "teleport-right4", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.RIGHT].shoot = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "shoot-right", delay: 500 }
        ]);

        CybertuxMini.animations[CybertuxDirection.RIGHT].hurted = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "hurted-right0", delay: 100 },
            { name: "hurted-right1", delay: 100 },
            { name: "hurted-right0", delay: 100 },
            { name: "hurted-right1", delay: 100 },
            { name: "hurted-right0", delay: 100 },
            { name: "hurted-right1", delay: 100 },
            { name: "hurted-right0", delay: 100 },
            { name: "hurted-right1", delay: 100 },
            { name: "hurted-right0", delay: 100 },
            { name: "hurted-right1", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.LEFT].aboutToExplode = CybertuxMini.animations[CybertuxDirection.RIGHT].aboutToExplode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "about-to-explode0", delay: 100 },
            { name: "about-to-explode1", delay: 100 },
            { name: "about-to-explode0", delay: 100 },
            { name: "about-to-explode1", delay: 100 },
            { name: "about-to-explode0", delay: 100 },
            { name: "about-to-explode1", delay: 100 },
            { name: "about-to-explode0", delay: 100 },
            { name: "about-to-explode1", delay: 100 },
            { name: "about-to-explode0", delay: 100 },
            { name: "about-to-explode1", delay: 100 }
        ]);

        CybertuxMini.animations[CybertuxDirection.LEFT].explode = CybertuxMini.animations[CybertuxDirection.RIGHT].explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "explode0", delay: 100 },
            { name: "explode1", delay: 100 },
            { name: "explode2", delay: 100 },
            { name: "explode3", delay: 100 },
            { name: "explode4", delay: 100 }
        ]);

        CybertuxMini.bulletAnimations = new BulletAnimations();
        CybertuxMini.bulletAnimations.grow = CybertuxMini.bulletAnimations.fly = CybertuxMini.bulletAnimations.explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullet0", delay: 100 },
            { name: "bullet1", delay: 100 }
        ]);
    }

    static Unload() {
        CybertuxMini.animations = null;
        CybertuxMini.bulletAnimations = null;
    }

}

class MissileSighAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    sightAim;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    sightLock;
}

/**
 * @readonly
 * @enum {number}
 */
const MissileSightState = {
    INACTIVE: 1,
    AIM: 2,
    LOCKABLE: 3,
    LOCKED: 4
};

class MissileSight {
    /**
     * @type MissileSighAnimations
     */
    static animations;

    /**
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type MissileSightState
     */
    state;

    /**
     * @type Playnewton.CLOCK_Timer
     */
    lockTimer = new Playnewton.CLOCK_Timer(3000);

    speed = 8;
    lockDistance = 10;

    constructor() {
        this.state = MissileSightState.INACTIVE;

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.BULLETS);
    }

    get lockable() {
        return this.state === MissileSightState.LOCKABLE;
    }

    Lock() {
        if (this.state === MissileSightState.LOCKABLE) {
            this.lockTimer.Start();
            this.state = MissileSightState.LOCKED;
        }
    }

    Aim() {
        if (this.state === MissileSightState.INACTIVE) {
            this.state = MissileSightState.AIM;
            Playnewton.GPU.SetSpritePosition(this.sprite, Math.random() < 0.5 ? 0 : Playnewton.GPU.screenWidth, Math.random() < 0.5 ? 0 : Playnewton.GPU.screenHeight);
            Playnewton.GPU.EnableSprite(this.sprite);
            Playnewton.GPU.SetSpriteAnimation(this.sprite, MissileSight.animations.sightAim);
        }
    }

    /**
     * 
     * @param {Sara} sara 
     */
    Pursue(sara) {
        switch (this.state) {
            case MissileSightState.AIM:
                let dx = sara.sprite.centerX - this.sprite.centerX;
                let dy = sara.sprite.centerY - this.sprite.centerY;
                let distance = Math.sqrt(dx ** 2 + dy ** 2);
                if (distance < this.lockDistance) {
                    Playnewton.GPU.SetSpriteAnimation(this.sprite, MissileSight.animations.sightLock);
                    this.state = MissileSightState.LOCKABLE;
                } else {
                    let speed = this.speed / distance;
                    dx *= speed;
                    dy *= speed;
                    this.sprite.centerX += dx;
                    this.sprite.centerY += dy;
                }
                break;
            case MissileSightState.LOCKED:
                if (this.lockTimer.elapsed) {
                    this.state = MissileSightState.INACTIVE;
                    Playnewton.GPU.DisableSprite(this.sprite);
                }
                break;
        }
    }
}

/**
 * @readonly
 * @enum {number}
 */
const MissileState = {
    INACTIVE: 1,
    FLY: 2,
    EXPLODE: 3
};

class MissileAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    missileFly;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    missileExplode;
}

class Missile {

    /**
     * @type MissileAnimations
     */
    static animations;

    /**
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type MissileState
     */
    state;

    /**
     * @type number
     */
    targetX;

    /**
     * @type number
     */
    targetY;

    explodeDistance = 8;
    speed = 8;

    constructor() {
        this.state = MissileState.INACTIVE;

        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.BULLETS);
    }

    get launchable() {
        return this.state == MissileState.INACTIVE;
    }

    /**
     * 
     * @param {number} x 
     * @param {number} y 
     */
    LaunchTo(x, y) {
        if (this.launchable) {
            this.targetX = x;
            this.targetY = y;
            this.state = MissileState.FLY;
            Playnewton.GPU.SetSpritePosition(this.sprite, Math.random() < 0.5 ? 0 : Playnewton.GPU.screenWidth, Math.random() < 0.5 ? 0 : Playnewton.GPU.screenHeight);
            Playnewton.GPU.EnableSprite(this.sprite);
            Playnewton.GPU.SetSpriteAnimation(this.sprite, Missile.animations.missileFly);
        }
    }

    /**
     * @param {Sara} sara 
     */
    Pursue(sara) {
        switch (this.state) {
            case MissileState.FLY:
                {
                    let dx = this.targetX - this.sprite.centerX;
                    let dy = this.targetY - this.sprite.centerY;
                    let distance = Math.sqrt(dx ** 2 + dy ** 2);
                    if (distance < this.explodeDistance) {
                        Playnewton.GPU.SetSpriteAnimation(this.sprite, Missile.animations.missileExplode, Playnewton.GPU_AnimationMode.ONCE);
                        this.state = MissileState.EXPLODE;
                    } else {
                        let speed = this.speed / distance;
                        dx *= speed;
                        dy *= speed;
                        this.sprite.centerX += dx;
                        this.sprite.centerY += dy;
                        this.sprite.angle = Math.atan2(dy, dx);
                    }
                }
                break;
            case MissileState.EXPLODE:
                {
                    if (this.sprite.animationStopped) {
                        Playnewton.GPU.DisableSprite(this.sprite);
                        this.state = MissileState.INACTIVE;
                    } else {
                        let dx = sara.sprite.centerX - this.sprite.centerX;
                        let dy = sara.sprite.centerY - this.sprite.centerY;
                        let distance = Math.sqrt(dx ** 2 + dy ** 2);
                        if (distance < this.sprite.width) {
                            sara.HurtByBullet();
                        }
                    }
                }
                break;
        }
    }
}

class MissileLauncherDescription {
    /**
     * @type number
     */
    nbMissiles;

    /**
     * @type number
     */
    nbSights;
}

class MissileLauncher {

    /**
     * @type Array<MissileSight>
     */
    sights = [];

    /**
     * @type Array<Missile>
     */
    missiles = [];

    /**
     * 
     * @param {MissileLauncherDescription} desc 
     */
    constructor(desc) {
        for (let s = 0; s < desc.nbSights; ++s) {
            this.sights.push(new MissileSight());
        }
        for (let s = 0; s < desc.nbMissiles; ++s) {
            this.missiles.push(new Missile());
        }
    }

    Aim() {
        for (let s of this.sights) {
            s.Aim();
        }
    }

    /**
     * 
     * @param {Sara} sara 
     */
    Pursue(sara) {
        for (let s of this.sights) {
            s.Pursue(sara);
            if (s.lockable) {
                for (let m of this.missiles) {
                    if (m.launchable) {
                        s.Lock();
                        m.LaunchTo(s.sprite.centerX, s.sprite.centerY);
                        break;
                    }
                }
            }
        }
        for (let m of this.missiles) {
            m.Pursue(sara);
        }
    }

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/city/cybertux.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "sight-aim0", x: 643, y: 658, w: 64, h: 64 },
            { name: "sight-aim1", x: 708, y: 658, w: 64, h: 64 },
            { name: "sight-aim2", x: 773, y: 658, w: 64, h: 64 },
            { name: "sight-aim3", x: 838, y: 658, w: 64, h: 64 },
            { name: "sight-lock0", x: 903, y: 658, w: 64, h: 64 },
            { name: "missile-fly0", x: 643, y: 723, w: 50, h: 22 },
            { name: "missile-fly1", x: 694, y: 723, w: 50, h: 22 },
            { name: "missile-fly2", x: 745, y: 723, w: 50, h: 22 },
            { name: "missile-fly3", x: 796, y: 723, w: 50, h: 22 },
            { name: "missile-explode0", x: 643, y: 747, w: 64, h: 64 },
            { name: "missile-explode1", x: 708, y: 747, w: 64, h: 64 },
            { name: "missile-explode2", x: 773, y: 747, w: 64, h: 64 },
            { name: "missile-explode3", x: 838, y: 747, w: 64, h: 64 }

        ]);

        MissileSight.animations = new MissileSighAnimations();

        MissileSight.animations.sightAim = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "sight-aim0", delay: 100 },
            { name: "sight-aim1", delay: 100 },
            { name: "sight-aim2", delay: 100 },
            { name: "sight-aim3", delay: 100 }
        ]);

        MissileSight.animations.sightLock = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "sight-aim0", delay: 100 },
            { name: "sight-lock0", delay: 100 },
            { name: "sight-aim0", delay: 100 },
            { name: "sight-lock0", delay: 100 },
            { name: "sight-aim0", delay: 100 },
            { name: "sight-lock0", delay: 100 },
            { name: "sight-aim0", delay: 100 },
            { name: "sight-lock0", delay: 100 },
            { name: "sight-aim0", delay: 100 },
            { name: "sight-lock0", delay: 100 }
        ]);

        Missile.animations = new MissileAnimations();

        Missile.animations.missileFly = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "missile-fly0", delay: 100 },
            { name: "missile-fly1", delay: 100 },
            { name: "missile-fly2", delay: 100 },
            { name: "missile-fly3", delay: 100 }
        ]);

        Missile.animations.missileExplode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "missile-explode0", delay: 100 },
            { name: "missile-explode1", delay: 100 },
            { name: "missile-explode2", delay: 100 },
            { name: "missile-explode3", delay: 100 }
        ]);
    }

    static Unload() {
        Missile.animations = null;
        MissileSight.animations = null;
    }

}
