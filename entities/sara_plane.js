import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import { BulletAnimations, Gun } from "./bullet.js";
import Enemy from "./enemy.js";
import ExplosionArea from "./explosion_area.js";
import Vulnerable from "./vulnerable.js";

/**
 * @readonly
 * @enum {number}
 */
const SaraPlaneState = {
    FLY: 1,
    DYING: 2,
};

/**
 * 
 * @type SaraPlaneAnimations
 */
class SaraPlaneAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    fly;


    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    hurt;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    dying;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    explosion;

    /**
     * @type BulletAnimations
     */
    bullets;
}

export default class SaraPlane extends Vulnerable {
    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     *  @type number
     */
    flySpeed = 2;

    /**
     *  @type SaraPlaneState
     */
    state = SaraPlaneState.FLY;

    /**
     * @type number
     */
    health = 5;

    /**
     * @type number
     */
    maxHealth = 10;

    get dead() {
        return this.health <= 0;
    }

    /**
     * @type {Gun}
     */
    gun;

    /**
     * @type Playnewton.CLOCK_Timer
     */
    hurtTimer = new Playnewton.CLOCK_Timer(1000);

    /**
     * @type ExplosionArea
     */
    explosions;

    /**
     * @type SaraPlaneAnimations
     */
    static animations;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/seaplane.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "fly0", x: 1, y: 1, w: 191, h: 109 },
            { name: "fly1", x: 1, y: 111, w: 191, h: 109 },
            { name: "fly2", x: 1, y: 221, w: 191, h: 109 },
            { name: "fly3", x: 1, y: 331, w: 191, h: 109 },
            { name: "fly3", x: 1, y: 331, w: 191, h: 109 },
            { name: "fly4", x: 193, y: 1, w: 191, h: 109 },
            { name: "fly5", x: 193, y: 111, w: 191, h: 109 },
            { name: "fly6", x: 193, y: 221, w: 191, h: 109 },
            { name: "hurt", x: 193, y: 331, w: 191, h: 109 },
            { name: "bullets0", x: 1, y: 448, w: 21, h: 7 },
            { name: "bullets1", x: 23, y: 448, w: 21, h: 7 },
            { name: "bullets2", x: 45, y: 448, w: 21, h: 7 },
            { name: "bullets3", x: 67, y: 448, w: 21, h: 7 },
            { name: "explosion0", x: 384, y: 0, w: 32, h: 32 },
            { name: "explosion1", x: 384, y: 32, w: 32, h: 32 },
            { name: "explosion2", x: 384, y: 64, w: 32, h: 32 },
            { name: "explosion3", x: 384, y: 96, w: 32, h: 32 }
        ]);

        SaraPlane.animations = new SaraPlaneAnimations();

        SaraPlane.animations.fly = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "fly0", delay: 50 },
            { name: "fly1", delay: 50 },
            { name: "fly2", delay: 50 },
            { name: "fly3", delay: 50 },
            { name: "fly4", delay: 50 },
            { name: "fly5", delay: 50 },
            { name: "fly6", delay: 50 },
        ]);

        SaraPlane.animations.hurt = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "fly0", delay: 50 },
            { name: "hurt", delay: 50 },
            { name: "fly1", delay: 50 },
            { name: "hurt", delay: 50 },
            { name: "fly2", delay: 50 },
            { name: "hurt", delay: 50 },
            { name: "fly3", delay: 50 },
            { name: "hurt", delay: 50 },
            { name: "fly4", delay: 50 },
            { name: "hurt", delay: 50 },
            { name: "fly5", delay: 50 },
            { name: "hurt", delay: 50 },
            { name: "fly6", delay: 50 },
            { name: "hurt", delay: 50 }
        ]);

        SaraPlane.animations.bullets = new BulletAnimations();
        SaraPlane.animations.bullets.grow = SaraPlane.animations.bullets.fly = SaraPlane.animations.bullets.explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullets0", delay: 50 },
            { name: "bullets1", delay: 50 },
            { name: "bullets2", delay: 50 },
            { name: "bullets3", delay: 50 }
        ]);

        SaraPlane.animations.explosion = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "explosion0", delay: 100 },
            { name: "explosion1", delay: 100 },
            { name: "explosion2", delay: 100 },
            { name: "explosion3", delay: 100 }
        ]);

    }

    static Unload() {
        SaraPlane.animations = null;
    }

    constructor(x, y) {
        super();
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, SaraPlane.animations.fly);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.SARA);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 0, 0, 115, 25);
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        Playnewton.PPU.SetBodyCollideWorldBounds(this.body, true);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -8, 8, -8, 8);
        Playnewton.PPU.EnableBody(this.body);

        this.gun = new Gun({animations: SaraPlane.animations.bullets, fireSound: "sounds/shoot-sara.wav", maxBullets: 10, maxBulletsPerShot: 1, delayBetweenTwoShotsInMs: 100, growSpeed: 1});
    }

    UpdateBody() {
        if (this.state === SaraPlaneState.DYING) {
            return;
        }

        let pad = Playnewton.CTRL.GetMasterPad();

        if (pad.A) {
            this.gun.fire(this.body.position.x - 63 + 172, this.body.position.y - 40 + 75, this.flySpeed * 4, 0);
        }

        let velocityX = this.body.velocity.x;
        let velocityY = this.body.velocity.y;

        if (pad.up) {
            velocityY -= this.flySpeed;
        } else if (pad.down) {
            velocityY += this.flySpeed;
        } else {
            velocityY = 0;
        }

        if (pad.left) {
            velocityX -= this.flySpeed;
        } else if (pad.right) {
            velocityX += this.flySpeed;
        } else {
            velocityX = 0;
        }

        Playnewton.PPU.SetBodyVelocity(this.body, velocityX, velocityY);
        this.gun.UpdateBody();
    }

    UpdateSprite() {
        switch (this.state) {
            case SaraPlaneState.FLY:
                if(this.hurtTimer.elapsed) {
                    Playnewton.GPU.SetSpriteAnimation(this.sprite, SaraPlane.animations.fly);
                } else {
                    Playnewton.GPU.SetSpriteAnimation(this.sprite, SaraPlane.animations.hurt);
                }
                break;
            case SaraPlaneState.DYING:
                Playnewton.PPU.SetBodyCollisionMask(this.body, 0);
                Playnewton.GPU.SetSpriteAnimation(this.sprite, SaraPlane.animations.hurt);
                break;
        }
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x - 63, this.body.position.y - 40);
        this.gun.UpdateSprite();
        if(this.explosions) {
            this.explosions.Update();
        }
    }

    CollectOneHeart() {
        if (!this.dead) {
            this.health = Math.min(this.health + 1, this.maxHealth);
        }
    }

    HurtByBullet() {
        this.Hurt();
        //TODO add hurted animation / movement?
    }

    HurtByEnemy() {
        this.Hurt();
        //TODO add hurted animation / movement?
    }

    get bulletable() {
        return true;
    }

    /**
     * @param {Enemy} enemy 
     */
    Pursue(enemy) {
        this.gun.Pursue(enemy);
    }

    Hurt() {
        if (this.dead || !this.hurtTimer.elapsed) {
            return;
        }
        this.hurtTimer.Start();
        this.health = Math.max(this.health - 1, 0);
        if (this.dead) {
            this.state = SaraPlaneState.DYING;
            let area = new Playnewton.PPU_Rectangle();
            area.x = this.body.left;
            area.y = this.body.top;
            area.w = this.body.width;
            area.h = this.body.height;
            this.explosions = new ExplosionArea(area, SaraPlane.animations.explosion);
            Playnewton.PPU.SetBodyImmovable(this.body, true);
        } else {
            Playnewton.APU.PlaySound("sounds/hurt-sara.wav");
        }
    }

}
