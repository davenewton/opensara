import Enemy from "./enemy.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js";
import Sara from "./sara.js";
import { Bullet, BulletAnimations } from "./bullet.js";

/**
 * @readonly
 * @enum {number}
 */
const BaldState = {
    IDLE: 1,
    ATTACK: 2,
    HURT: 3,
    DYING: 4,
    DEAD: 5
};

/**
 * 
 * @type BaldAnimations
 */
class BaldAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    idle;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    attack;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    hurt;
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    die;
}

export default class Bald extends Enemy {

    /**
     * 
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * 
     * @type Playnewton.PPU_Body
     */
    body;

    /**
     *  @type BaldState
     */
    state;

    /**
     * @type number
     */
    health = 3;

    get dead() {
        return this.health <= 0;
    }

    /**
     * @type Array<Bullet>
     */
    bullets = [];

    /**
     * @type number
     */
    flySpeed = 1;

    /**
     * @type number
     */
    fleeSpeed = 4;

    /**
     * @type BaldAnimations
     */
    static animations;

    /**
     * @type BulletAnimations
     */
    static bulletAnimations;

    /**
     * @type Playnewton.PPU_Point
     */
    destination;

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/bald.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "idle00", x: 1, y: 1, w: 46, h: 53 },
            { name: "attack00", x: 49, y: 1, w: 46, h: 53 },
            { name: "hurt00", x: 1, y: 56, w: 46, h: 53 },
            { name: "hurt01", x: 49, y: 56, w: 46, h: 53 },
            { name: "bullet-fly00", x: 97, y: 1, w: 14, h: 14 },
            { name: "bullet-fly01", x: 113, y: 1, w: 14, h: 14 },
            { name: "bullet-fly02", x: 97, y: 17, w: 14, h: 14 },
            { name: "bullet-fly03", x: 113, y: 17, w: 14, h: 14 },
            { name: "bullet-fly04", x: 97, y: 33, w: 14, h: 14 },
            { name: "bullet-fly05", x: 113, y: 33, w: 14, h: 14 },
            { name: "bullet-fly06", x: 97, y: 49, w: 14, h: 14 },
            { name: "bullet-fly07", x: 113, y: 49, w: 14, h: 14 },
            { name: "bullet-fly08", x: 97, y: 65, w: 14, h: 14 },
            { name: "bullet-explode00", x: 97, y: 81, w: 14, h: 14 },
            { name: "bullet-explode01", x: 113, y: 81, w: 14, h: 14 },
            { name: "bullet-explode02", x: 97, y: 97, w: 14, h: 14 },
            { name: "bullet-explode03", x: 113, y: 97, w: 14, h: 14 },
            { name: "bullet-explode04", x: 97, y: 113, w: 14, h: 14 },
            { name: "bullet-explode05", x: 113, y: 113, w: 14, h: 14 },
        ]);

        Bald.animations = new BaldAnimations();

        Bald.animations.idle = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "idle00", delay: 1000 }
        ]);

        Bald.animations.attack = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "attack00", delay: 1000 }
        ]);

        Bald.animations.die = Bald.animations.hurt = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 },
            { name: "hurt00", delay: 100 },
            { name: "hurt01", delay: 100 }
        ]);

        Bald.bulletAnimations = new BulletAnimations();

        Bald.bulletAnimations.grow = Bald.bulletAnimations.fly = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullet-fly00", delay: 25 },
            { name: "bullet-fly01", delay: 25 },
            { name: "bullet-fly02", delay: 25 },
            { name: "bullet-fly03", delay: 25 },
            { name: "bullet-fly04", delay: 25 },
            { name: "bullet-fly05", delay: 25 },
            { name: "bullet-fly06", delay: 25 },
            { name: "bullet-fly07", delay: 25 },
            { name: "bullet-fly08", delay: 25 }
        ]);

        Bald.bulletAnimations.explode = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "bullet-explode00", delay: 100 },
            { name: "bullet-explode01", delay: 100 },
            { name: "bullet-explode02", delay: 100 },
            { name: "bullet-explode03", delay: 100 },
            { name: "bullet-explode04", delay: 100 },
            { name: "bullet-explode05", delay: 100 }
        ]);
    }

    static Unload() {
        Bald.animations = null;
        Bald.bulletAnimations = null;
    }

    constructor(x, y, xDest, yDest) {
        super();
        this.destination = new Playnewton.PPU_Point(xDest, yDest);
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Bald.animations.idle);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.ENEMIES);
        Playnewton.GPU.EnableSprite(this.sprite);

        this.body = Playnewton.PPU.CreateBody();
        Playnewton.PPU.SetBodyRectangle(this.body, 0, 0, 14, 28);
        Playnewton.PPU.SetBodyPosition(this.body, x, y);
        Playnewton.PPU.SetBodyVelocityBounds(this.body, -this.flySpeed, this.flySpeed, -this.flySpeed, this.flySpeed);
        Playnewton.PPU.EnableBody(this.body);

        this.state = BaldState.IDLE;

        for (let i = 0; i < 10; i++) {
            this.bullets.push(new Bullet(Bald.bulletAnimations));
        }
    }

    UpdateSprite() {
        switch (this.state) {
            case BaldState.IDLE:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Bald.animations.idle, Playnewton.GPU_AnimationMode.ONCE);
                break;
            case BaldState.ATTACK:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Bald.animations.attack, Playnewton.GPU_AnimationMode.ONCE);
                if (this.sprite.animationStopped) {
                    Playnewton.GPU.SetSpriteAnimation(this.sprite, Bald.animations.idle, Playnewton.GPU_AnimationMode.ONCE);
                    this.state = BaldState.IDLE;
                }
                break;
            case BaldState.HURT:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Bald.animations.hurt, Playnewton.GPU_AnimationMode.ONCE);
                if (this.sprite.animationStopped) {
                    Playnewton.GPU.SetSpriteAnimation(this.sprite, Bald.animations.idle, Playnewton.GPU_AnimationMode.ONCE);
                    this.state = BaldState.IDLE;
                }
                break;
            case BaldState.DYING:
                Playnewton.GPU.SetSpriteAnimation(this.sprite, Bald.animations.die, Playnewton.GPU_AnimationMode.ONCE);
                break;
        }
        Playnewton.GPU.SetSpritePosition(this.sprite, this.body.position.x - 17, this.body.position.y - 8);
        for (let bullet of this.bullets) {
            bullet.UpdateSprite();
        }
    }

    UpdateBody() {
        switch (this.state) {
            case BaldState.IDLE:
                this.flyAround();
                break;
            case BaldState.ATTACK:
                Playnewton.PPU.SetBodyVelocity(this.body, 0, 0);
                break;
            case BaldState.HURT:
                break;
            case BaldState.DYING:
                Playnewton.PPU.SetBodyVelocityBounds(this.body, -this.fleeSpeed, this.fleeSpeed, -this.fleeSpeed, this.fleeSpeed);
                Playnewton.PPU.SetBodyVelocity(this.body, this.fleeSpeed, 0);
                if (!Playnewton.PPU.CheckIfBodyIsInWorldBound(this.body)) {
                    Playnewton.GPU.DisableSprite(this.sprite);
                    Playnewton.PPU.DisableBody(this.body);
                    this.state = BaldState.DEAD;
                }
                break;
            case BaldState.DEAD:
                break;
        }
        for (let bullet of this.bullets) {
            bullet.UpdateBody();
        }
    }

    flyAround() {
        let dx = this.destination.x - this.body.centerX;
        let dy = this.destination.y - this.body.centerY;
        let distance = Math.sqrt(dx ** 2 + dy ** 2);
        if(distance < 32) {
            this.destination.y = Math.random() * (Playnewton.GPU.screenHeight - this.sprite.height * 4) + this.sprite.height * 2;
        }
        let s = this.flySpeed / distance;
        dx *= s;
        dy *= s;
        Playnewton.PPU.SetBodyVelocity(this.body, dx, dy);
    }

    get bulletable() {
        return true;
    }

    /**
     * 
     * @param {Sara} sara 
     */
    Pursue(sara) {
        switch (this.state) {
            case BaldState.IDLE:
                if (this.sprite.animationStopped) {
                    for (let bullet of this.bullets) {
                        if (bullet.canBeFired) {
                            Playnewton.APU.PlaySound("sounds/shoot-bald.wav");
                            bullet.fireAt(this.body.position.x + 20, this.body.position.y - 6, sara.body.centerX, sara.body.centerY, 2);
                            this.state = BaldState.ATTACK;
                            break;
                        }
                    }
                }
                break;
        }
        for (let bullet of this.bullets) {
            bullet.Pursue(sara);
        }
    }

    Hurt() {
        switch (this.state) {
            case BaldState.IDLE:
            case BaldState.ATTACK:
                Playnewton.APU.PlaySound("sounds/hurt-enemy.wav");
                this.state = BaldState.HURT;
                this.health = Math.max(this.health - 1, 0);
                if (this.dead) {
                    this.state = BaldState.DYING;
                }
            break;
        }
    }
}