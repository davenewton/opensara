import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import Sara from "../entities/sara.js"
import Z_ORDER from "../utils/z_order.js"
import Fadeout from "../entities/fadeout.js"
import { IngameMapKeyboardEventToPadButton, MenuMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js"
import Announcement from "../entities/announcement.js"
import Cybertux from "../entities/cybertux.js"
import { Dialog } from "../entities/dialog.js"

/**
 * Enum for level state
 * @readonly
 * @enum {number}
 */
const CityCybertuxLevelState = {
    START: 1,
    PLAY: 2,
    END: 7,
    GAME_OVER: 8,
    STOPPED: 9
};

class MenuItem {
    /**
     * @type string
     */
    text;

    /**
     * @type Playnewton.GPU_Label
     */
    label;

    constructor(text) {
        this.text = text;
        this.label = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.label, "bold 24px monospace");
        Playnewton.GPU.HUD.SetLabelText(this.label, text);
        Playnewton.GPU.HUD.SetLabelAlign(this.label, "right");
        Playnewton.GPU.HUD.EnableLabel(this.label);
    }

    Trigger() {
    }

    /**
     * 
     * @param {boolean} selected 
     */
    Update(selected) {
        if (selected) {
            Playnewton.GPU.HUD.SetLabelText(this.label, `👉${this.text}`);
        } else {
            Playnewton.GPU.HUD.SetLabelText(this.label, this.text);
        }
    }
}

class Menu {

    /**
     * @type Array<MenuItem>
     */
    items = [];

    /**
     * @type number
     */
    currentItemIndex = 0;

    AddItem(item) {
        this.items.push(item);
    }

    Update() {
        let pad = Playnewton.CTRL.GetMasterPad();
        if (pad.TestStartAndResetIfPressed()) {
            Playnewton.APU.PlaySound("sounds/pause_resume.wav");
            let selectedItem = this.items[this.currentItemIndex];
            selectedItem.Trigger();
        }

        if (pad.TestUpAndResetIfPressed()) {
            --this.currentItemIndex;
            Playnewton.APU.PlaySound("sounds/menu-select.wav");
        }
        if (pad.TestDownAndResetIfPressed()) {
            ++this.currentItemIndex;
            pad.downWasNotPressed = false;
            Playnewton.APU.PlaySound("sounds/menu-select.wav");
        }
        this.currentItemIndex = Playnewton.FPU.wrap(0, this.currentItemIndex, this.items.length - 1);
        this.items.forEach((item, index) => {
            item.Update(index === this.currentItemIndex);
        });
    }
}

export default class CityCybertuxLevel extends Scene {

    /**
     * @type CityCybertuxLevelState
     */
    state;

    /**
     * @type Sara
     */
    sara;

    /**
     * @type Playnewton.GPU_Bar
     */
    saraHealthBar;

    /**
     * @type Playnewton.GPU_Bar
     */
    cybertuxHealthBar;

    /**
     * @type string
     */
    mapPath;

    /**
     * @type Scene
     */
    nextSceneOnExit;

    /**
     * @type Fadeout
     */
    fadeout;

    /**
     * @type Playnewton.GPU_Label
     */
    skipLabel;

    /**
     * @type Playnewton.GPU_Label
     */
    dialogLabel;

    /**
     * @type Dialog
     */
    dialog;

    /**
     * @type Announcement
     */
    levelAnnouncement;

    /**
     * @type Cybertux
     */
    cybertux;

    /**
     * @type Menu
     */
    menu;

    /**
     * 
     * @param {string} mapPath 
     * @param {Scene} nextSceneOnExit 
     */
    constructor(mapPath, nextSceneOnExit) {
        super();
        this.mapPath = mapPath;
        this.nextSceneOnExit = nextSceneOnExit;
        this.pausable = false;
    }

    async InitMap() {
        let map = await Playnewton.DRIVE.LoadTmxMap(this.mapPath);

        Playnewton.PPU.SetWorldBounds(16, 16, Playnewton.GPU.screenWidth - 32, Playnewton.GPU.screenHeight - 32);
        Playnewton.PPU.SetWorldGravity(0, 1);

        Playnewton.DRIVE.ConvertTmxMapToGPUSprites(Playnewton.GPU, map, 0, 0, Z_ORDER.BACKGROUND);
        Playnewton.DRIVE.ConvertTmxMapToPPUBodies(Playnewton.PPU, map, 0, 0);

        await this.InitMapObjects(map);
    }

    /**
     * 
     * @param {Playnewton.TMX_Map} map 
     */
    async InitMapObjects(map) {
        Playnewton.DRIVE.ForeachTmxMapObject(
            (object, objectgroup, x, y) => {
                switch (object.type) {
                    case "sara":
                        if (!this.sara) {
                            this.sara = new Sara(x, y);
                            this.sara.Wait();
                        }
                        break;
                    case "cybertux":
                        this.cybertux = new Cybertux(x, y);
                        break;
                    default:
                        break;
                }
            },
            map);

        if (this.cybertux) {
            Playnewton.DRIVE.ForeachTmxMapObject(
                (object, objectgroup, x, y) => {
                    switch (object.type) {
                        case "cybertuxmini":
                            this.cybertux.AddMini(x, y);
                            break;
                        default:
                            break;
                    }
                },
                map);
        }
    }


    InitHUD() {
        this.saraHealthBar = Playnewton.GPU.HUD.CreateBar();
        Playnewton.GPU.HUD.SetBarPosition(this.saraHealthBar, 10, 10);
        Playnewton.GPU.HUD.SetBarSize(this.saraHealthBar, this.sara.maxHealth);
        Playnewton.GPU.HUD.SetBarLevel(this.saraHealthBar, this.sara.health);
        Playnewton.GPU.HUD.EnableBar(this.saraHealthBar);

        Playnewton.GPU.EnableHUD(true);

        this.cybertuxHealthBar = Playnewton.GPU.HUD.CreateBar();
        Playnewton.GPU.HUD.SetBarPosition(this.cybertuxHealthBar, 900, 10);
        Playnewton.GPU.HUD.SetBarSize(this.cybertuxHealthBar, this.cybertux.maxHealth);
        Playnewton.GPU.HUD.SetBarLevel(this.cybertuxHealthBar, this.cybertux.health);
        Playnewton.GPU.HUD.SetBarStyle(this.cybertuxHealthBar, 8, 16, 4, "#101820", "#9898c8", "#b8006f");
        Playnewton.GPU.HUD.EnableBar(this.cybertuxHealthBar);

        Playnewton.GPU.EnableHUD(true);
    }

    async Start() {
        await super.Start();

        this.pausable = false;
        this.state = CityCybertuxLevelState.START;

        this.fadeout = null;

        Playnewton.CTRL.MapKeyboardEventToPadButton = IngameMapKeyboardEventToPadButton;
        let audioPromise = Promise.allSettled([
            Playnewton.APU.PlayMusic("musics/centurion_of_war/helldrake.ogg"),
            Playnewton.APU.Preload("sounds/jump-sara.wav"),
            Playnewton.APU.Preload("sounds/hurt-sara.wav"),
            Playnewton.APU.Preload("sounds/shoot-cybertuxmini.wav"),
            Playnewton.APU.Preload("sounds/tripleshoot-cybertux.wav"),
            Playnewton.APU.Preload("sounds/sideshoot-cybertux.wav"),
            Playnewton.APU.Preload("musics/centurion_of_war/deviation2.ogg")
        ]);
        this.progress = 0;

        for (let z = Z_ORDER.MIN; z <= Z_ORDER.MAX; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }
        this.progress = 10;

        await Sara.Load();
        this.progress = 20;

        await Cybertux.Load();
        this.progress = 30;

        await this.InitMap();
        this.progress = 70;

        this.InitHUD();
        this.progress = 90;

        await audioPromise;
        this.progress = 100;
    }

    Stop() {
        super.Stop();
        Sara.Unload();
        this.sara = null;

        Cybertux.Unload();
        this.cybertux = null;

        this.fadeout = null;
    }

    UpdateBodies() {

        let pad = Playnewton.CTRL.GetMasterPad();
        switch (this.state) {
            case CityCybertuxLevelState.START:
                this.levelAnnouncement = new Announcement("Cybertux");
                this.levelAnnouncement.Start();
                this.sara.StopWaiting();
                this.state = CityCybertuxLevelState.PLAY;
                break;
            case CityCybertuxLevelState.PLAY:
                this.cybertux.Pursue(this.sara);
                this.sara.Stomp(this.cybertux);
                for (let mini of this.cybertux.minis) {
                    this.sara.Stomp(mini);
                }
                this.handleSaraDeath();
                this.handleCybertuxDeath();
                break;
            case CityCybertuxLevelState.END:
                this.dialog.Update(this.dialogLabel);
                if (pad.TestStartAndResetIfPressed()) {
                    Playnewton.APU.PlaySound("sounds/skip.wav");
                    this.dialog.Skip();
                }
                if (this.dialog.done) {
                    this.fadeoutToNextScene();
                }
                break;
            case CityCybertuxLevelState.GAME_OVER:
                return;
            case CityCybertuxLevelState.STOPPED:
                return;
        }
        this.cybertux.UpdateBody();
        this.sara.UpdateBody();
    }

    handleSaraDeath() {
        if (this.sara.dead) {
            Playnewton.CTRL.MapKeyboardEventToPadButton = MenuMapKeyboardEventToPadButton;
            this.state = CityCybertuxLevelState.GAME_OVER;
            let layers = [];
            for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
                if (i !== Z_ORDER.SARA) {
                    layers.push(i);
                }
            }
            this.fadeout = new Fadeout(1000, layers, () => {
                Playnewton.APU.PlayMusic("musics/centurion_of_war/chippy_melodyv2.ogg");

                this.pausable = false;
                let gameoverLabel = Playnewton.GPU.HUD.CreateLabel();
                Playnewton.GPU.HUD.SetLabelFont(gameoverLabel, "bold 48px monospace");
                Playnewton.GPU.HUD.SetLabelColor(gameoverLabel, "#ff0000");
                Playnewton.GPU.HUD.SetLabelAlign(gameoverLabel, "center");
                Playnewton.GPU.HUD.SetLabelPosition(gameoverLabel, 512, 256);
                Playnewton.GPU.HUD.SetLabelText(gameoverLabel, "Game over");
                Playnewton.GPU.HUD.EnableLabel(gameoverLabel);

                Playnewton.GPU.HUD.DisableBar(this.saraHealthBar);
                Playnewton.GPU.HUD.DisableBar(this.cybertuxHealthBar);

                let self = this;
                this.menu = new Menu();
                this.menu.AddItem(new class extends MenuItem {
                    constructor() {
                        super("Retry");
                        Playnewton.GPU.HUD.SetLabelPosition(this.label, 544, 320);
                    }

                    Trigger() {
                        self.state = CityCybertuxLevelState.STOPPED;
                        self.Stop();
                        self.nextScene = self;
                        self.Start();
                    }
                });
                this.menu.AddItem(new class extends MenuItem {
                    constructor() {
                        super("Give up");
                        Playnewton.GPU.HUD.SetLabelPosition(this.label, 544, 352);
                    }

                    Trigger() {
                        self.state = CityCybertuxLevelState.STOPPED;
                        self.Stop();
                        self.nextScene = self.nextSceneOnExit;
                        self.nextSceneOnExit.Start();
                    }
                });
            });
        }
    }

    handleCybertuxDeath() {
        if (this.cybertux.dead) {
            this.dialogLabel = Playnewton.GPU.HUD.CreateLabel();
            Playnewton.GPU.HUD.SetLabelFont(this.dialogLabel, "bold 32px monospace");
            Playnewton.GPU.HUD.SetLabelAlign(this.dialogLabel, "left");
            Playnewton.GPU.HUD.SetLabelPosition(this.dialogLabel, 32, Playnewton.GPU.screenHeight - 44);
            Playnewton.GPU.HUD.SetLabelText(this.dialogLabel, "");
            Playnewton.GPU.HUD.SetLabelBackground(this.dialogLabel, 0, Playnewton.GPU.screenHeight - 88, Playnewton.GPU.screenWidth, 88, `#000000cc`);
            Playnewton.GPU.HUD.EnableLabel(this.dialogLabel);

            this.skipLabel = Playnewton.GPU.HUD.CreateLabel();
            Playnewton.GPU.HUD.SetLabelFont(this.skipLabel, "bold 12px monospace");
            Playnewton.GPU.HUD.SetLabelAlign(this.skipLabel, "right");
            Playnewton.GPU.HUD.SetLabelPosition(this.skipLabel, 1024, 564);
            Playnewton.GPU.HUD.SetLabelColor(this.skipLabel, "#eeeeee");
            Playnewton.GPU.HUD.SetLabelText(this.skipLabel, "Skip with ⌨️enter or 🎮start");
            Playnewton.GPU.HUD.EnableLabel(this.skipLabel);

            this.dialog = new Dialog();
            Playnewton.APU.PlayMusic("musics/centurion_of_war/deviation2.ogg");
            this.dialog.Start([
                { color: "#8fffff", text: "[Sara] Let's go back to the present !", delay: 1000 },
                { color: "#8fffff", text: "[Sara] Or maybe to the future ?", delay: 5000 },
                { color: "#ffffff", text: "THE END", speed: 200, align: "center", x: 512, y: 532, delay: 10000 }
            ]);
            this.state = CityCybertuxLevelState.END;
        }
    }

    fadeoutToNextScene() {
        if (!this.fadeout) {
            let layers = [];
            for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
                layers.push(i);
            }
            this.fadeout = new Fadeout(1000, layers, () => {
                this.state = CityCybertuxLevelState.STOPPED;
                this.Stop();
                this.nextScene = this.nextSceneOnExit;
                this.nextSceneOnExit.Start();
            });
        }
    }

    UpdateSprites() {
        if (this.state === CityCybertuxLevelState.STOPPED) {
            return;
        }
        this.cybertux.UpdateSprite();
        this.sara.UpdateSprite();

        Playnewton.GPU.HUD.SetBarLevel(this.saraHealthBar, this.sara.health);
        Playnewton.GPU.HUD.SetBarLevel(this.cybertuxHealthBar, this.cybertux.health);

        if (this.levelAnnouncement) {
            this.levelAnnouncement.Update();
        }
        if (this.fadeout) {
            this.fadeout.Update();
        }
        if(this.menu) {
            this.menu.Update();
        }
    }
}
