import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import Z_ORDER from "../utils/z_order.js"
import Fadeout from "../entities/fadeout.js"
import { Dialog } from "../entities/dialog.js"
import Announcement from "../entities/announcement.js"
import Sara from "../entities/sara.js"
import Annie from "../entities/annie.js"
import { IngameMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js"

/**
 * Enum for level state
 * @readonly
 * @enum {number}
 */
const TrainLevelState = {
    START: 0,
    INTRO_DIALOG1: 1,
    INTRO_DIALOG2: 2,
    FIGHT: 3,
    ANNIE_CRY: 4,
    SARA_APPROACH_ANNIE_CRYING: 5,
    OUTRO_DIALOG: 6,
    END: 7,
    GAME_OVER: 14,
    STOPPED: 15
};

const NB_SCREEN = 4;

export default class TrainLevel extends Scene {

    /**
     * @type TrainLevelState
     */
    state;

    /**
     * @type Sara
     */
    sara;

    /**
     * @type Annie
     */
    annie;

    /**
     * @type Playnewton.GPU_Bar
     */
    saraHealthBar;

    /**
     * @type Playnewton.GPU_Bar
     */
    annieHealthBar;

    /**
     * @type Scene
     */
    nextSceneOnExit;

    /**
     * @type Fadeout
     */
    fadeout;

    /**
     * @type number
     */
    skyBackgroundScrollXSpeed = 0.125;

    /**
     * @type Array<Playnewton.GPU_Sprite>
     */
    skyBackgroundSprites = [];

    /**
     * @type number
     */
    seaBackgroundScrollXSpeed = 0.25;

    /**
     * @type Array<Playnewton.GPU_Sprite>
     */
    seaBackgroundSprites = [];

    /**
     * @type number
     */
    tracksForegroundScrollXSpeed = 8;

    /**
     * @type number
     */
    signalisationScrollXSpeed = 8;

    /**
     * @type Array<Playnewton.GPU_Sprite>
     */
    tracksForegroundSprites = [];

    /**
* @type Array<Playnewton.GPU_Sprite>
*/
    signalisationSprites = [];

    /**
     * @type number
     */
    islandScrollXSpeed = 0.2;

    /**
     * @type Array<Playnewton.GPU_Sprite>
     */
    islandSprites = [];

    /**
     * @type Array<Playnewton.GPU_Sprite>
     */
    wheelSprites = [];

    /**
     * @type Playnewton.GPU_Label
     */
    dialogLabel;

    /**
     * @type Playnewton.GPU_Label
     */
    skipLabel;

    /**
     * @type Dialog
     */
    dialog;

    constructor(nextSceneOnExit) {
        super();
        this.nextSceneOnExit = nextSceneOnExit;
    }

    async InitWorld() {
        for (let z = Z_ORDER.MIN; z <= Z_ORDER.MAX; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }

        Playnewton.PPU.SetWorldBounds(0, 0, Playnewton.GPU.screenWidth * NB_SCREEN, 576);
        Playnewton.PPU.SetWorldGravity(0, 1);

        let trainBitmap = await Playnewton.DRIVE.LoadBitmap("maps/train/train.png");

        let skyBackgroundPicture = Playnewton.GPU.CreatePicture(trainBitmap, 0, 0, 1024, 448);
        let seaBackgroundPicture = Playnewton.GPU.CreatePicture(trainBitmap, 0, 448, 1024, 128);
        let tracksForegroundPicture = Playnewton.GPU.CreatePicture(trainBitmap, 0, 610, 1024, 53);
        let signalisationPicture = Playnewton.GPU.CreatePicture(trainBitmap, 1, 664, 32, 316);
        let smallIslandPicture = Playnewton.GPU.CreatePicture(trainBitmap, 187, 588, 93, 21);
        let mediumIslandPicture = Playnewton.GPU.CreatePicture(trainBitmap, 1, 581, 185, 28);
        let bigIslandPicture = Playnewton.GPU.CreatePicture(trainBitmap, 281, 577, 247, 32);

        let windturbineSpriteSet = Playnewton.GPU.CreateSpriteset(trainBitmap, [
            { name: "small0", x: 34, y: 884, w: 21, h: 32 },
            { name: "small1", x: 56, y: 884, w: 21, h: 32 },
            { name: "small2", x: 78, y: 884, w: 21, h: 32 },
            { name: "small3", x: 100, y: 884, w: 21, h: 32 },
            { name: "small4", x: 122, y: 884, w: 21, h: 32 },
            { name: "small5", x: 144, y: 884, w: 21, h: 32 },
            { name: "small6", x: 166, y: 884, w: 21, h: 32 },
            { name: "small7", x: 188, y: 884, w: 21, h: 32 },
            { name: "big0", x: 34, y: 835, w: 32, h: 48 },
            { name: "big1", x: 67, y: 835, w: 32, h: 48 },
            { name: "big2", x: 100, y: 835, w: 32, h: 48 },
            { name: "big3", x: 133, y: 835, w: 32, h: 48 },
            { name: "big4", x: 166, y: 835, w: 32, h: 48 },
            { name: "big5", x: 199, y: 835, w: 32, h: 48 },
            { name: "big6", x: 232, y: 835, w: 32, h: 48 },
            { name: "big7", x: 265, y: 835, w: 32, h: 48 }
        ]);

        let smallWindturbineAnimation = Playnewton.GPU.CreateAnimation(windturbineSpriteSet, [
            { name: "small0", delay: 100 },
            { name: "small1", delay: 100 },
            { name: "small2", delay: 100 },
            { name: "small3", delay: 100 },
            { name: "small4", delay: 100 },
            { name: "small5", delay: 100 },
            { name: "small6", delay: 100 },
            { name: "small7", delay: 100 }
        ]);

        let bigWindturbineAnimation = Playnewton.GPU.CreateAnimation(windturbineSpriteSet, [
            { name: "big0", delay: 100 },
            { name: "big1", delay: 100 },
            { name: "big2", delay: 100 },
            { name: "big3", delay: 100 },
            { name: "big4", delay: 100 },
            { name: "big5", delay: 100 },
            { name: "big6", delay: 100 },
            { name: "big7", delay: 100 }
        ]);

        for (let screen = 0; screen < (NB_SCREEN + 1); screen++) {
            let skyBackgroundSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(skyBackgroundSprite, skyBackgroundPicture);
            Playnewton.GPU.SetSpritePosition(skyBackgroundSprite, screen * skyBackgroundPicture.w, 0);
            Playnewton.GPU.SetSpriteZ(skyBackgroundSprite, Z_ORDER.TRAIN_SKY_BACKGROUND);
            Playnewton.GPU.EnableSprite(skyBackgroundSprite);
            this.skyBackgroundSprites.push(skyBackgroundSprite);

            let seaBackgroundSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(seaBackgroundSprite, seaBackgroundPicture);
            Playnewton.GPU.SetSpritePosition(seaBackgroundSprite, screen * seaBackgroundPicture.w, 448);
            Playnewton.GPU.SetSpriteZ(seaBackgroundSprite, Z_ORDER.TRAIN_SEA_BACKGROUND);
            Playnewton.GPU.EnableSprite(seaBackgroundSprite);
            this.seaBackgroundSprites.push(seaBackgroundSprite);

            let tracksForegroundSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(tracksForegroundSprite, tracksForegroundPicture);
            Playnewton.GPU.SetSpritePosition(tracksForegroundSprite, screen * tracksForegroundPicture.w, 523);
            Playnewton.GPU.SetSpriteZ(tracksForegroundSprite, Z_ORDER.TRAIN_TRACKS_FOREGROUND);
            Playnewton.GPU.EnableSprite(tracksForegroundSprite);
            this.tracksForegroundSprites.push(tracksForegroundSprite);

            if (screen % 3 == 0) {
                let signalisationSprite = Playnewton.GPU.CreateSprite();
                Playnewton.GPU.SetSpritePicture(signalisationSprite, signalisationPicture);
                Playnewton.GPU.SetSpritePosition(signalisationSprite, tracksForegroundSprite.x, tracksForegroundSprite.y - signalisationPicture.h);
                Playnewton.GPU.SetSpriteZ(signalisationSprite, Z_ORDER.TRAIN_TRACKS_FOREGROUND);
                Playnewton.GPU.EnableSprite(signalisationSprite);
                this.signalisationSprites.push(signalisationSprite);
            }

            if (screen % 2 == 0) {
                let mediumIslandSprite = Playnewton.GPU.CreateSprite();
                Playnewton.GPU.SetSpritePicture(mediumIslandSprite, mediumIslandPicture);
                Playnewton.GPU.SetSpritePosition(mediumIslandSprite, screen * 1024, 448 - mediumIslandPicture.h);
                Playnewton.GPU.SetSpriteZ(mediumIslandSprite, Z_ORDER.TRAIN_ISLAND_BACKGROUND);
                Playnewton.GPU.EnableSprite(mediumIslandSprite);
                this.islandSprites.push(mediumIslandSprite);

                let bigWindTurbineOnMediumIslandSprite = Playnewton.GPU.CreateSprite();
                Playnewton.GPU.SetSpriteAnimation(bigWindTurbineOnMediumIslandSprite, bigWindturbineAnimation);
                Playnewton.GPU.SetSpritePosition(bigWindTurbineOnMediumIslandSprite, mediumIslandSprite.x + 90, mediumIslandSprite.y - 48);
                Playnewton.GPU.SetSpriteZ(bigWindTurbineOnMediumIslandSprite, Z_ORDER.TRAIN_ISLAND_BACKGROUND);
                Playnewton.GPU.EnableSprite(bigWindTurbineOnMediumIslandSprite);
                this.islandSprites.push(bigWindTurbineOnMediumIslandSprite);

                let smallIslandSprite = Playnewton.GPU.CreateSprite();
                Playnewton.GPU.SetSpritePicture(smallIslandSprite, smallIslandPicture);
                Playnewton.GPU.SetSpritePosition(smallIslandSprite, screen * 1024 + 240, 448 - smallIslandPicture.h);
                Playnewton.GPU.SetSpriteZ(smallIslandSprite, Z_ORDER.TRAIN_ISLAND_BACKGROUND);
                Playnewton.GPU.EnableSprite(smallIslandSprite);
                this.islandSprites.push(smallIslandSprite);

                let smallWindTurbineOnSmallIslandSprite = Playnewton.GPU.CreateSprite();
                Playnewton.GPU.SetSpriteAnimation(smallWindTurbineOnSmallIslandSprite, smallWindturbineAnimation);
                Playnewton.GPU.SetSpritePosition(smallWindTurbineOnSmallIslandSprite, smallIslandSprite.x + 34, smallIslandSprite.y - 32);
                Playnewton.GPU.SetSpriteZ(smallWindTurbineOnSmallIslandSprite, Z_ORDER.TRAIN_ISLAND_BACKGROUND);
                Playnewton.GPU.EnableSprite(smallWindTurbineOnSmallIslandSprite);
                this.islandSprites.push(smallWindTurbineOnSmallIslandSprite);

                let bigIslandSprite = Playnewton.GPU.CreateSprite();
                Playnewton.GPU.SetSpritePicture(bigIslandSprite, bigIslandPicture);
                Playnewton.GPU.SetSpritePosition(bigIslandSprite, screen * 1024 + 600, 448 - bigIslandPicture.h);
                Playnewton.GPU.SetSpriteZ(bigIslandSprite, Z_ORDER.TRAIN_ISLAND_BACKGROUND);
                Playnewton.GPU.EnableSprite(bigIslandSprite);
                this.islandSprites.push(bigIslandSprite);

                let bigWindTurbineOnBigIslandSprite = Playnewton.GPU.CreateSprite();
                Playnewton.GPU.SetSpriteAnimation(bigWindTurbineOnBigIslandSprite, bigWindturbineAnimation);
                Playnewton.GPU.SetSpritePosition(bigWindTurbineOnBigIslandSprite, bigIslandSprite.x + 137, bigIslandSprite.y - 48);
                Playnewton.GPU.SetSpriteZ(bigWindTurbineOnBigIslandSprite, Z_ORDER.TRAIN_ISLAND_BACKGROUND);
                Playnewton.GPU.EnableSprite(bigWindTurbineOnBigIslandSprite);
                this.islandSprites.push(bigWindTurbineOnBigIslandSprite);

                let smallWindTurbineOnBigIslandSprite = Playnewton.GPU.CreateSprite();
                Playnewton.GPU.SetSpriteAnimation(smallWindTurbineOnBigIslandSprite, smallWindturbineAnimation);
                Playnewton.GPU.SetSpritePosition(smallWindTurbineOnBigIslandSprite, bigIslandSprite.x + 60, bigIslandSprite.y - 12);
                Playnewton.GPU.SetSpriteZ(smallWindTurbineOnBigIslandSprite, Z_ORDER.TRAIN_ISLAND_BACKGROUND);
                Playnewton.GPU.EnableSprite(smallWindTurbineOnBigIslandSprite);
                this.islandSprites.push(smallWindTurbineOnBigIslandSprite);
            }
        }

        let junctionPicture = Playnewton.GPU.CreatePicture(trainBitmap, 1003, 577, 20, 15);

        /**
          * @param {number} x 
          */
        let AddJunction = (x) => {
            let junctionSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(junctionSprite, junctionPicture);
            Playnewton.GPU.SetSpritePosition(junctionSprite, x, 498);
            Playnewton.GPU.SetSpriteZ(junctionSprite, Z_ORDER.TRAIN_FRONT_BACKGROUND);
            Playnewton.GPU.EnableSprite(junctionSprite);
        }

        let smallWheelPicture = Playnewton.GPU.CreatePicture(trainBitmap, 975, 947, 48, 48);
        let bigWheelPicture = Playnewton.GPU.CreatePicture(trainBitmap, 959, 882, 64, 64);

        /**
          * @param {number} x 
          */
        let AddSmallWheelAt = (x) => {
            let wheelSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wheelSprite, smallWheelPicture);
            Playnewton.GPU.SetSpritePosition(wheelSprite, x, 478);
            Playnewton.GPU.SetSpriteZ(wheelSprite, Z_ORDER.TRAIN_WHEELS_BACKGROUND);
            Playnewton.GPU.EnableSprite(wheelSprite);
            this.wheelSprites.push(wheelSprite);
        }

        /**
         * @param {number} x
         */
        let AddBigWheelAt = (x) => {
            let wheelSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wheelSprite, bigWheelPicture);
            Playnewton.GPU.SetSpritePosition(wheelSprite, x, 462);
            Playnewton.GPU.SetSpriteZ(wheelSprite, Z_ORDER.TRAIN_WHEELS_BACKGROUND);
            Playnewton.GPU.EnableSprite(wheelSprite);
            this.wheelSprites.push(wheelSprite);
        }

        /**
         * @param {number} x 
         */
        let AddWagonWheels = (x) => {
            AddSmallWheelAt(x);
            AddSmallWheelAt(x + 64);
            AddSmallWheelAt(x + 173);
            AddSmallWheelAt(x + 173 + 64);
        }

        let wagonBackPicture = Playnewton.GPU.CreatePicture(trainBitmap, 529, 577, 289, 32);

        let crateWagonFrontPicture = Playnewton.GPU.CreatePicture(trainBitmap, 317, 836, 319, 165);
        let AddCrateWagon = () => {
            let wagonBackSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wagonBackSprite, wagonBackPicture);
            Playnewton.GPU.SetSpritePosition(wagonBackSprite, 458, 476);
            Playnewton.GPU.SetSpriteZ(wagonBackSprite, Z_ORDER.TRAIN_BACK_BACKGROUND);
            Playnewton.GPU.EnableSprite(wagonBackSprite);

            let wagonFrontSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wagonFrontSprite, crateWagonFrontPicture);
            Playnewton.GPU.SetSpritePosition(wagonFrontSprite, 443, 344);
            Playnewton.GPU.SetSpriteZ(wagonFrontSprite, Z_ORDER.TRAIN_FRONT_BACKGROUND);
            Playnewton.GPU.EnableSprite(wagonFrontSprite);
            AddWagonWheels(459);

            let wagonBody = Playnewton.PPU.CreateBody();
            Playnewton.PPU.SetBodyPosition(wagonBody, 458, 344);
            Playnewton.PPU.SetBodyRectangle(wagonBody, 0, 0, 278, 161);
            Playnewton.PPU.SetBodyImmovable(wagonBody, true);
            Playnewton.PPU.EnableBody(wagonBody);

            AddJunction(754);
        }
        AddCrateWagon();

        let plankWagonFrontPicture = Playnewton.GPU.CreatePicture(trainBitmap, 637, 882, 319, 75);
        let AddPlankWagon = () => {
            let wagonBackSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wagonBackSprite, wagonBackPicture);
            Playnewton.GPU.SetSpritePosition(wagonBackSprite, 781, 476);
            Playnewton.GPU.SetSpriteZ(wagonBackSprite, Z_ORDER.TRAIN_BACK_BACKGROUND);
            Playnewton.GPU.EnableSprite(wagonBackSprite);

            let wagonFrontSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wagonFrontSprite, plankWagonFrontPicture);
            Playnewton.GPU.SetSpritePosition(wagonFrontSprite, 766, 433);
            Playnewton.GPU.SetSpriteZ(wagonFrontSprite, Z_ORDER.TRAIN_FRONT_BACKGROUND);
            Playnewton.GPU.EnableSprite(wagonFrontSprite);
            AddWagonWheels(782);

            let wagonBody = Playnewton.PPU.CreateBody();
            Playnewton.PPU.SetBodyPosition(wagonBody, 784, 432);
            Playnewton.PPU.SetBodyRectangle(wagonBody, 0, 0, 283, 21);
            Playnewton.PPU.SetBodyImmovable(wagonBody, true);
            Playnewton.PPU.EnableBody(wagonBody);

            AddJunction(1077);
        }
        AddPlankWagon();

        let passengerWagonFrontPicture = Playnewton.GPU.CreatePicture(trainBitmap, 75, 664, 319, 170);
        let AddPassengerWagon = () => {
            let wagonBackSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wagonBackSprite, wagonBackPicture);
            Playnewton.GPU.SetSpritePosition(wagonBackSprite, 1104, 476);
            Playnewton.GPU.SetSpriteZ(wagonBackSprite, Z_ORDER.TRAIN_BACK_BACKGROUND);
            Playnewton.GPU.EnableSprite(wagonBackSprite);

            let wagonFrontSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wagonFrontSprite, passengerWagonFrontPicture);
            Playnewton.GPU.SetSpritePosition(wagonFrontSprite, 1089, 339);
            Playnewton.GPU.SetSpriteZ(wagonFrontSprite, Z_ORDER.TRAIN_FRONT_BACKGROUND);
            Playnewton.GPU.EnableSprite(wagonFrontSprite);
            AddWagonWheels(1105);

            let wagonBody = Playnewton.PPU.CreateBody();
            Playnewton.PPU.SetBodyPosition(wagonBody, 1104, 339);
            Playnewton.PPU.SetBodyRectangle(wagonBody, 0, 0, 290, 116);
            Playnewton.PPU.SetBodyImmovable(wagonBody, true);
            Playnewton.PPU.EnableBody(wagonBody);

            AddJunction(1400);
        }
        AddPassengerWagon();

        let tenderWagonFrontPicture = Playnewton.GPU.CreatePicture(trainBitmap, 395, 664, 238, 171);
        let tenderWagonBackPicture = Playnewton.GPU.CreatePicture(trainBitmap, 529, 577, 208, 8);
        let AddTengerWagon = () => {
            let wagonBackSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wagonBackSprite, tenderWagonBackPicture);
            Playnewton.GPU.SetSpritePosition(wagonBackSprite, 1427, 500);
            Playnewton.GPU.SetSpriteZ(wagonBackSprite, Z_ORDER.TRAIN_BACK_BACKGROUND);
            Playnewton.GPU.EnableSprite(wagonBackSprite);

            let wagonFrontSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(wagonFrontSprite, tenderWagonFrontPicture);
            Playnewton.GPU.SetSpritePosition(wagonFrontSprite, 1412, 338);
            Playnewton.GPU.SetSpriteZ(wagonFrontSprite, Z_ORDER.TRAIN_FRONT_BACKGROUND);
            Playnewton.GPU.EnableSprite(wagonFrontSprite);
            AddSmallWheelAt(1428);
            AddSmallWheelAt(1491);
            AddSmallWheelAt(1584);

            let wagonBody = Playnewton.PPU.CreateBody();
            Playnewton.PPU.SetBodyPosition(wagonBody, 1425, 338);
            Playnewton.PPU.SetBodyRectangle(wagonBody, 0, 0, 209, 116);
            Playnewton.PPU.SetBodyImmovable(wagonBody, true);
            Playnewton.PPU.EnableBody(wagonBody);

            AddJunction(1642);
        }
        AddTengerWagon();

        let locomotiveFrontPicture = Playnewton.GPU.CreatePicture(trainBitmap, 634, 664, 389, 217);
        let locomotiveBackPicture = Playnewton.GPU.CreatePicture(trainBitmap, 529, 577, 315, 32);
        let AddLocomotive = () => {
            let locomotiveBackSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(locomotiveBackSprite, locomotiveBackPicture);
            Playnewton.GPU.SetSpritePosition(locomotiveBackSprite, 1687, 474);
            Playnewton.GPU.SetSpriteZ(locomotiveBackSprite, Z_ORDER.TRAIN_BACK_BACKGROUND);
            Playnewton.GPU.EnableSprite(locomotiveBackSprite);

            let locomotiveFrontSprite = Playnewton.GPU.CreateSprite();
            Playnewton.GPU.SetSpritePicture(locomotiveFrontSprite, locomotiveFrontPicture);
            Playnewton.GPU.SetSpritePosition(locomotiveFrontSprite, 1661, 303);
            Playnewton.GPU.SetSpriteZ(locomotiveFrontSprite, Z_ORDER.TRAIN_FRONT_BACKGROUND);
            Playnewton.GPU.EnableSprite(locomotiveFrontSprite);
            AddBigWheelAt(1690);
            AddBigWheelAt(1786);
            AddSmallWheelAt(1882);
            AddSmallWheelAt(1946);

            let cabBody = Playnewton.PPU.CreateBody();
            Playnewton.PPU.SetBodyPosition(cabBody, 1665, 337);
            Playnewton.PPU.SetBodyRectangle(cabBody, 0, 0, 55, 118);
            Playnewton.PPU.SetBodyImmovable(cabBody, true);
            Playnewton.PPU.EnableBody(cabBody);

            let locomotiveBody = Playnewton.PPU.CreateBody();
            Playnewton.PPU.SetBodyPosition(locomotiveBody, 1720, 342);
            Playnewton.PPU.SetBodyRectangle(locomotiveBody, 0, 0, 274, 81);
            Playnewton.PPU.SetBodyImmovable(locomotiveBody, true);
            Playnewton.PPU.EnableBody(locomotiveBody);
        }
        AddLocomotive();
    }

    InitHUD() {
        this.saraHealthBar = Playnewton.GPU.HUD.CreateBar();
        Playnewton.GPU.HUD.SetBarPosition(this.saraHealthBar, 10, 10);
        Playnewton.GPU.HUD.SetBarSize(this.saraHealthBar, this.sara.maxHealth);
        Playnewton.GPU.HUD.SetBarLevel(this.saraHealthBar, this.sara.health);
        Playnewton.GPU.HUD.EnableBar(this.saraHealthBar);

        this.annieHealthBar = Playnewton.GPU.HUD.CreateBar();
        Playnewton.GPU.HUD.SetBarPosition(this.annieHealthBar, 900, 10);
        Playnewton.GPU.HUD.SetBarSize(this.annieHealthBar, this.annie.maxHealth);
        Playnewton.GPU.HUD.SetBarLevel(this.annieHealthBar, this.annie.health);
        Playnewton.GPU.HUD.SetBarStyle(this.annieHealthBar, 8, 16, 4, "#101820", "#9898c8", "#b8006f");
        Playnewton.GPU.HUD.EnableBar(this.annieHealthBar);

        Playnewton.GPU.EnableHUD(true);
    }

    async InitDialog() {
        this.dialog = new Dialog();

        this.dialogLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.dialogLabel, "bold 32px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.dialogLabel, "left");
        Playnewton.GPU.HUD.SetLabelPosition(this.dialogLabel, 32, Playnewton.GPU.screenHeight - 44);
        Playnewton.GPU.HUD.SetLabelText(this.dialogLabel, "");
        Playnewton.GPU.HUD.SetLabelBackground(this.dialogLabel, 0, Playnewton.GPU.screenHeight - 88, Playnewton.GPU.screenWidth, 88, `#000000cc`);
        Playnewton.GPU.HUD.EnableLabel(this.dialogLabel);

        this.skipLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.skipLabel, "bold 12px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.skipLabel, "right");
        Playnewton.GPU.HUD.SetLabelPosition(this.skipLabel, 1024, 564);
        Playnewton.GPU.HUD.SetLabelColor(this.skipLabel, "#eeeeee");
        Playnewton.GPU.HUD.SetLabelText(this.skipLabel, "Skip with ⌨️enter or 🎮start");
        Playnewton.GPU.HUD.EnableLabel(this.skipLabel);
    }

    async Start() {
        this.state = TrainLevelState.START;

        this.fadeout = null;

        await super.Start();

        Playnewton.CTRL.MapKeyboardEventToPadButton = IngameMapKeyboardEventToPadButton;

        let audioPromise = Promise.allSettled([
            Playnewton.APU.PlayMusic("musics/centurion_of_war/guitar_53.ogg"),
            Playnewton.APU.Preload("musics/centurion_of_war/chippy_melodyv2.ogg"),
            Playnewton.APU.Preload("musics/centurion_of_war/helldrake.ogg"),
            Playnewton.APU.Preload("musics/centurion_of_war/deviation2.ogg"),
            Playnewton.APU.Preload("sounds/hurt-sara.wav"),
            Playnewton.APU.Preload("sounds/shoot-annie.wav")
        ]);
        this.progress = 0;

        await this.InitWorld();
        this.progress = 10;

        await this.InitDialog();
        this.progress = 20;

        await Sara.Load();
        this.sara = new Sara(1687, 290);
        this.progress = 40;

        await Annie.Load();
        this.annie = new Annie(1800, 286);
        this.annie.attackPlatforms = [
            //crate wagon
            {
                minX: 470,
                maxX: 740
            },

            //plank wagon
            {
                minX: 790,
                maxX: 1070
            },

            //passenger wagon
            {
                minX: 1110,
                maxX: 1390
            },

            //tender
            {
                minX: 1430,
                maxX: 1640
            },

            //locomotive
            {
                minX: 1740,
                maxX: 1990
            }
        ];

        this.InitHUD();
        this.progress = 95;

        await audioPromise;
        this.progress = 100;
    }

    Stop() {
        super.Stop();

        Sara.Unload();
        this.sara = null;

        Annie.Unload();
        this.annie = null;

        this.skyBackgroundSprites = [];
        this.tracksForegroundSprites = [];
        this.signalisationSprites = [];
        this.islandSprites = [];
        this.wheelSprites = [];
    }

    UpdateBodies() {
        let pad = Playnewton.CTRL.GetMasterPad();

        switch (this.state) {
            case TrainLevelState.START:
                this.pausable = false;
                this.sara.Wait();
                this.dialog.Start([
                    { color: "#8fffff", text: "[Sara] I've been hired to protect this train." },
                    { color: "#8fffff", text: "[Sara] But I dont expect any attack." },
                ]);
                this.state = TrainLevelState.INTRO_DIALOG1;
                break;
            case TrainLevelState.INTRO_DIALOG1:
                this.dialog.Update(this.dialogLabel);
                if (pad.TestStartAndResetIfPressed()) {
                    Playnewton.APU.PlaySound("sounds/skip.wav");
                    this.dialog.Skip();
                }
                if (this.dialog.done) {
                    this.dialog.Start([
                        { color: "#ff0000", text: "[Annie] Nobody expect Annie attack !" }
                    ]);
                    this.annie.HailSara();
                    this.state = TrainLevelState.INTRO_DIALOG2;
                }
                break;
            case TrainLevelState.INTRO_DIALOG2:
                this.dialog.Update(this.dialogLabel);
                if (pad.TestStartAndResetIfPressed()) {
                    Playnewton.APU.PlaySound("sounds/skip.wav");
                    this.dialog.Skip();
                }
                if (this.dialog.done && this.annie.body.touches.bottom) {
                    Playnewton.GPU.HUD.DisableLabel(this.dialogLabel);
                    Playnewton.GPU.HUD.DisableLabel(this.skipLabel);
                    Playnewton.APU.PlayMusic("musics/centurion_of_war/helldrake.ogg");
                    this.sara.StopWaiting();
                    this.annie.CrouchAndJump();
                    this.state = TrainLevelState.FIGHT;
                }
                break;
            case TrainLevelState.FIGHT:
                this.annie.Pursue(this.sara);
                this.handleSaraDeath();
                if (this.annie.dead) {
                    this.state = TrainLevelState.ANNIE_CRY;
                }
                break;
            case TrainLevelState.ANNIE_CRY:
                if(this.annie.crying) {
                    Playnewton.APU.PlayMusic("musics/centurion_of_war/deviation2.ogg");
                    this.state = TrainLevelState.SARA_APPROACH_ANNIE_CRYING;
                }
                break;
            case TrainLevelState.SARA_APPROACH_ANNIE_CRYING:
                if (Math.abs(this.sara.body.centerX - this.annie.body.centerX) < 200) {
                    this.dialog.Start([
                        { color: "#ff0000", text: "[Annie] Don't think this means you've won!" },
                        { color: "#8fffff", text: "[Sara] Be reasonable Annie." },
                        { color: "#ff0000", text: "[Annie] I tried being reasonable..." },
                        { color: "#ff0000", text: "[Annie] But I didn't like it." },
                        { color: "#ffffff", text: "THE END", speed: 200, align: "center", x: 512, y: 532, delay: 10000 }
                    ]);
                    Playnewton.GPU.HUD.EnableLabel(this.dialogLabel);
                    Playnewton.GPU.HUD.EnableLabel(this.skipLabel);
                    
                    this.state = TrainLevelState.OUTRO_DIALOG;
                }
                break;
            case TrainLevelState.OUTRO_DIALOG:
                this.dialog.Update(this.dialogLabel);
                if (pad.TestStartAndResetIfPressed()) {
                    Playnewton.APU.PlaySound("sounds/skip.wav");
                    this.dialog.Skip();
                }
                if (this.dialog.done) {
                    this.state = TrainLevelState.END;
                    this.fadeoutToNextScene();
                }
                break;
            case TrainLevelState.END:
                return;
            case TrainLevelState.GAME_OVER:
                this.pausable = false;
                if (pad.TestStartAndResetIfPressed()) {
                    Playnewton.APU.PlaySound("sounds/pause_resume.wav");
                    this.state = TrainLevelState.STOPPED;
                    this.Stop();
                    this.nextScene = this.nextSceneOnExit;
                    this.nextSceneOnExit.Start();
                }
                return;
            case TrainLevelState.STOPPED:
                return;
        }
        if (this.sara.body.top >= 523) {
            this.sara.Hurt();
            if (this.sara.dead) {
                Playnewton.PPU.SetBodyPosition(this.sara.body, 1687, 290);
            } else {
                Playnewton.PPU.SetBodyPosition(this.sara.body, 1687, 0);
            }
        }
        this.sara.UpdateBody();
        this.sara.Stomp(this.annie);
        this.annie.LookAtSara(this.sara);
        this.annie.UpdateBody();
    }

    handleSaraDeath() {
        if (this.sara.dead) {
            let layers = [];
            for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
                if (i !== Z_ORDER.SARA && i !== Z_ORDER.EXPLOSION) {
                    layers.push(i);
                }
            }
            this.fadeout = new Fadeout(1000, layers, () => {
                Playnewton.APU.PlayMusic("musics/centurion_of_war/chippy_melodyv2.ogg");

                let gameoverLabel = Playnewton.GPU.HUD.CreateLabel();
                Playnewton.GPU.HUD.SetLabelFont(gameoverLabel, "bold 48px monospace");
                Playnewton.GPU.HUD.SetLabelColor(gameoverLabel, "#ff0000");
                Playnewton.GPU.HUD.SetLabelAlign(gameoverLabel, "center");
                Playnewton.GPU.HUD.SetLabelPosition(gameoverLabel, 512, 288);
                Playnewton.GPU.HUD.SetLabelText(gameoverLabel, "Game over");
                Playnewton.GPU.HUD.EnableLabel(gameoverLabel);
                Playnewton.GPU.HUD.DisableBar(this.saraHealthBar);
                Playnewton.GPU.HUD.DisableBar(this.annieHealthBar);

                let continueLabel = Playnewton.GPU.HUD.CreateLabel();
                Playnewton.GPU.HUD.SetLabelFont(continueLabel, "bold 12px monospace");
                Playnewton.GPU.HUD.SetLabelAlign(continueLabel, "right");
                Playnewton.GPU.HUD.SetLabelPosition(continueLabel, 1024, 564);
                Playnewton.GPU.HUD.SetLabelColor(continueLabel, "#eeeeee");
                Playnewton.GPU.HUD.SetLabelText(continueLabel, "Press ⌨️enter or 🎮start");
                Playnewton.GPU.HUD.EnableLabel(continueLabel);
            });
            this.state = TrainLevelState.GAME_OVER;
        }
    }

    fadeoutToNextScene() {
        if (!this.fadeout) {
            let layers = [];
            for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
                layers.push(i);
            }
            this.fadeout = new Fadeout(1000, layers, () => {
                this.Stop();
                this.nextScene = this.nextSceneOnExit;
                this.nextSceneOnExit.Start();
            });
        }
    }

    /**
     * 
     * @param {Array<Playnewton.GPU_Sprite>} sprites 
     * @param {number} dx 
     */
    WrapScroll(sprites, dx) {
        for (let sprite of sprites) {
            let x = sprite.x - dx;
            if (x <= -sprite.width) {
                x = Playnewton.GPU.screenWidth * (NB_SCREEN + 1) + x;
            }
            Playnewton.GPU.SetSpritePosition(sprite, x, sprite.y);
        }
    }

    UpdateSprites() {
        if (this.state === TrainLevelState.STOPPED) {
            return;
        }

        this.WrapScroll(this.skyBackgroundSprites, this.skyBackgroundScrollXSpeed);
        this.WrapScroll(this.seaBackgroundSprites, this.seaBackgroundScrollXSpeed);
        this.WrapScroll(this.islandSprites, this.islandScrollXSpeed);
        this.WrapScroll(this.tracksForegroundSprites, this.tracksForegroundScrollXSpeed);
        this.WrapScroll(this.signalisationSprites, this.signalisationScrollXSpeed);

        for (let wheel of this.wheelSprites) {
            wheel.angle += 0.14;
        }

        this.sara.UpdateSprite();
        Playnewton.GPU.HUD.SetBarLevel(this.saraHealthBar, this.sara.health);
        Playnewton.GPU.HUD.SetBarLevel(this.annieHealthBar, this.annie.health);

        this.annie.UpdateSprite();

        let scrollX = Playnewton.FPU.bound(-Playnewton.GPU.screenWidth * (NB_SCREEN - 1), -this.sara.sprite.x + 512, 0);
        Playnewton.GPU.SetScroll(scrollX, 0);

        if (this.fadeout) {
            this.fadeout.Update();
        }
    }
}
