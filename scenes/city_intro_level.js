import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import Sara from "../entities/sara.js"
import Z_ORDER from "../utils/z_order.js"
import Fadeout from "../entities/fadeout.js"
import { IngameMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js"
import Announcement from "../entities/announcement.js"
import { Dialog } from "../entities/dialog.js"

/**
 * Enum for level state
 * @readonly
 * @enum {number}
 */
const CityIntroLevelState = {
    START: 1,
    WAIT: 2,
    TELEPORT_KYLE: 3,
    KYLE_TALK: 4,
    MOVE_TELEPORTER: 5,
    WAIT_FOR_SARA: 6,
    TELEPORT_SARA: 7,
    END: 8,
    STOPPED: 9
};

class Kyle {

    /**
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    teleportAnimation;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    talkAnimation;
}

class Teleporter {
    /**
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    idleAnimation;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    teleportAnimation;
}

export default class CityIntroLevel extends Scene {

    /**
     * @type CityIntroLevelState
     */
    state;

    /**
     * @type Sara
     */
    sara;

    /**
     * @type string
     */
    mapPath;

    /**
     * @type Scene
     */
    nextSceneOnExit;

    /**
     * @type Fadeout
     */
    fadeout;

    /**
     * @type Playnewton.GPU_Label
     */
    skipLabel;

    /**
     * @type Playnewton.GPU_Label
     */
    dialogLabel;

    /**
     * @type Dialog
     */
    dialog;

    /**
     * @type Announcement
     */
    levelAnnouncement;

    /**
     * @type Kyle
     */
    kyle;

    /**
     * @type Teleporter
     */
    teleporter;

    /**
     * @type number
     */
    teleporterFinalX;

    /**
     * 
     * @param {string} mapPath 
     * @param {Scene} nextSceneOnExit 
     */
    constructor(mapPath, nextSceneOnExit) {
        super();
        this.mapPath = mapPath;
        this.nextSceneOnExit = nextSceneOnExit;
        this.pausable = false;
    }

    async InitMap() {
        let map = await Playnewton.DRIVE.LoadTmxMap(this.mapPath);

        Playnewton.PPU.SetWorldBounds(0, 0, Playnewton.GPU.screenWidth, Playnewton.GPU.screenHeight);
        Playnewton.PPU.SetWorldGravity(0, 1);

        Playnewton.DRIVE.ConvertTmxMapToGPUSprites(Playnewton.GPU, map, 0, 0, Z_ORDER.BACKGROUND);
        Playnewton.DRIVE.ConvertTmxMapToPPUBodies(Playnewton.PPU, map, 0, 0);

        await this.InitMapObjects(map);
    }

    /**
     * 
     * @param {Playnewton.TMX_Map} map 
     */
    async InitMapObjects(map) {
        Playnewton.DRIVE.ForeachTmxMapObject(
            (object, objectgroup, x, y) => {
                switch (object.type) {
                    case "sara":
                        if (!this.sara) {
                            this.sara = new Sara(x, y);
                            this.sara.Wait();
                        }
                        break;
                    case "kyle":
                        Playnewton.GPU.SetSpritePosition(this.kyle.sprite, x, y - 26);
                        Playnewton.GPU.SetSpriteZ(this.kyle.sprite, objectgroup.z);
                        
                        this.teleporterFinalX = x - 10 - 128;
                        Playnewton.GPU.SetSpritePosition(this.teleporter.sprite, x - 10, y - 64);
                        Playnewton.GPU.SetSpriteZ(this.teleporter.sprite, objectgroup.z);                      
                        break;
                    default:
                        break;
                }
            },
            map);
    }


    InitHUD() {
        this.dialogLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.dialogLabel, "bold 32px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.dialogLabel, "left");
        Playnewton.GPU.HUD.SetLabelPosition(this.dialogLabel, 32, Playnewton.GPU.screenHeight - 44);
        Playnewton.GPU.HUD.SetLabelText(this.dialogLabel, "");
        Playnewton.GPU.HUD.SetLabelBackground(this.dialogLabel, 0, Playnewton.GPU.screenHeight - 88, Playnewton.GPU.screenWidth, 88, `#000000cc`);

        this.skipLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.skipLabel, "bold 12px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.skipLabel, "right");
        Playnewton.GPU.HUD.SetLabelPosition(this.skipLabel, 1024, 564);
        Playnewton.GPU.HUD.SetLabelColor(this.skipLabel, "#eeeeee");
        Playnewton.GPU.HUD.SetLabelText(this.skipLabel, "Skip with ⌨️enter or 🎮start");
        Playnewton.GPU.HUD.EnableLabel(this.skipLabel);

        this.dialog = new Dialog();
        Playnewton.GPU.EnableHUD(true);
    }

    async InitKyle() {
        this.kyle = new Kyle();
        this.kyle.sprite = Playnewton.GPU.CreateSprite();

        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/city/sara_home.png");
        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "kyle0", x: 615, y: 577, w: 47, h: 26 },
            { name: "kyle1", x: 663, y: 577, w: 47, h: 26 },
            { name: "kyle2", x: 711, y: 577, w: 47, h: 26 },
            { name: "kyle3", x: 759, y: 577, w: 47, h: 26 },
            { name: "kyle4", x: 807, y: 577, w: 47, h: 26 },
            { name: "kyle5", x: 855, y: 577, w: 47, h: 26 },
            { name: "kyle6", x: 903, y: 577, w: 47, h: 26 },
        ]

        );
        this.kyle.teleportAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "kyle0", delay: 100 },
            { name: "kyle1", delay: 100 },
            { name: "kyle2", delay: 100 },
            { name: "kyle3", delay: 100 },
            { name: "kyle4", delay: 100 },
            { name: "kyle5", delay: 100 },
            { name: "kyle6", delay: 100 }
        ]);

        this.kyle.talkAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "kyle6", delay: 1000 }
        ]);
    }

    async InitTeleporter() {
        this.teleporter = new Teleporter();
        this.teleporter.sprite = Playnewton.GPU.CreateSprite();

        let bitmap = await Playnewton.DRIVE.LoadBitmap("maps/city/sara_home.png");
        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "idle0", x: 1, y: 735, w: 64, h: 23 },
            { name: "idle1", x: 66, y: 735, w: 64, h: 23 },
            { name: "idle2", x: 131, y: 735, w: 64, h: 23 },
            { name: "idle3", x: 196, y: 735, w: 64, h: 23 },
            { name: "idle4", x: 261, y: 735, w: 64, h: 23 },
            { name: "idle5", x: 326, y: 735, w: 64, h: 23 },
            { name: "idle6", x: 326, y: 735, w: 64, h: 23 },
            { name: "idle7", x: 391, y: 735, w: 64, h: 23 },
            { name: "teleport0", x: 456, y: 735, w: 64, h: 64 },
            { name: "teleport1", x: 521, y: 735, w: 64, h: 64 },
            { name: "teleport2", x: 586, y: 735, w: 64, h: 64 },
            { name: "teleport3", x: 651, y: 735, w: 64, h: 64 },
            { name: "teleport4", x: 716, y: 735, w: 64, h: 64 },
            { name: "teleport5", x: 781, y: 735, w: 64, h: 64 },
            { name: "teleport6", x: 846, y: 735, w: 64, h: 64 },
        ]
        );

        this.teleporter.idleAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "idle0", delay: 100 },
            { name: "idle1", delay: 100 },
            { name: "idle2", delay: 100 },
            { name: "idle3", delay: 100 },
            { name: "idle4", delay: 100 },
            { name: "idle5", delay: 100 },
            { name: "idle6", delay: 100 },
            { name: "idle7", delay: 100 },
            { name: "idle6", delay: 100 },
            { name: "idle5", delay: 100 },
            { name: "idle4", delay: 100 },
            { name: "idle3", delay: 100 },
            { name: "idle2", delay: 100 },
            { name: "idle1", delay: 100 },
        ]);

        this.teleporter.teleportAnimation = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "teleport0", delay: 100 },
            { name: "teleport1", delay: 100 },
            { name: "teleport2", delay: 100 },
            { name: "teleport3", delay: 100 },
            { name: "teleport4", delay: 100 },
            { name: "teleport5", delay: 100 },
            { name: "teleport6", delay: 100 }
        ]);
    }

    async Start() {
        this.pausable = false;
        this.state = CityIntroLevelState.START;

        this.fadeout = null;

        await super.Start();

        Playnewton.CTRL.MapKeyboardEventToPadButton = IngameMapKeyboardEventToPadButton;
        let audioPromise = Promise.allSettled([
            Playnewton.APU.Preload("sounds/jump-sara.wav"),
            Playnewton.APU.Preload("sounds/teleport.wav"),
            Playnewton.APU.PlayMusic("musics/centurion_of_war/deviation2.ogg")
        ]);
        this.progress = 0;

        for (let z = Z_ORDER.MIN; z <= Z_ORDER.MAX; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }
        this.progress = 10;

        await Sara.Load();
        this.progress = 20;

        await this.InitKyle();
        this.progress = 30;

        await this.InitTeleporter();
        this.progress = 40;

        await this.InitMap();
        this.progress = 70;

        this.InitHUD();
        this.progress = 90;

        await audioPromise;
        this.progress = 100;
    }

    Stop() {
        super.Stop();
        Sara.Unload();
        this.sara = null;

        this.kyle = null;
    }

    UpdateBodies() {

        let pad = Playnewton.CTRL.GetMasterPad();
        switch (this.state) {
            case CityIntroLevelState.START:
                this.levelAnnouncement = new Announcement("2023 : Sara's home");
                this.levelAnnouncement.Start();
                this.state = CityIntroLevelState.WAIT;
                break;
            case CityIntroLevelState.WAIT:
                if (this.levelAnnouncement.done) {
                    Playnewton.GPU.SetSpriteAnimation(this.teleporter.sprite, this.teleporter.teleportAnimation, Playnewton.GPU_AnimationMode.ONCE);
                    Playnewton.GPU.EnableSprite(this.teleporter.sprite);
                    Playnewton.GPU.SetSpriteAnimation(this.kyle.sprite, this.kyle.teleportAnimation, Playnewton.GPU_AnimationMode.ONCE);
                    Playnewton.GPU.EnableSprite(this.kyle.sprite);
                    Playnewton.APU.PlaySound("sounds/teleport.wav");
                    this.state = CityIntroLevelState.TELEPORT_KYLE;
                }
                break;
            case CityIntroLevelState.TELEPORT_KYLE:
                if (this.kyle.sprite.animationStopped) {
                    Playnewton.GPU.SetSpriteAnimation(this.teleporter.sprite, this.teleporter.idleAnimation);
                    Playnewton.GPU.SetSpriteAnimation(this.kyle.sprite, this.kyle.talkAnimation);
                    Playnewton.GPU.HUD.EnableLabel(this.dialogLabel);
                    this.dialog.Start([
                        { color: "#b3b9d1", text: "[Kyle] Sara ?" },
                        { color: "#b3b9d1", text: "[Kyle] I come from the future." },
                        { color: "#b3b9d1", text: "[Kyle] Penguin cyborgs control the world." },
                        { color: "#8fffff", text: "[Sara] What ?!?" },
                        { color: "#b3b9d1", text: "[Kyle] We should make a baby." },
                        { color: "#8fffff", text: "[Sara] Why ?!?" },
                        { color: "#b3b9d1", text: "[Kyle] He will grow up and save the future !" },
                        { color: "#8fffff", text: "[Sara] Or I could use your teleporter..." },
                        { color: "#8fffff", text: "[Sara] and save the future myself." }
                    ]);
                    this.state = CityIntroLevelState.KYLE_TALK;
                }
                break;
            case CityIntroLevelState.KYLE_TALK:
                this.dialog.Update(this.dialogLabel);
                if (this.dialog.done) {
                    this.state = CityIntroLevelState.MOVE_TELEPORTER;
                }
                break;
            case CityIntroLevelState.MOVE_TELEPORTER:
                if(this.teleporter.sprite.x <= this.teleporterFinalX) {
                    this.sara.StopWaiting();
                    this.state = CityIntroLevelState.WAIT_FOR_SARA;
                } else {
                    this.teleporter.sprite.x -= 1;
                }
                break;
            case CityIntroLevelState.WAIT_FOR_SARA:
                if(Math.abs(this.teleporter.sprite.centerX - this.sara.sprite.centerX) < 4 &&
                    this.sara.sprite.top > this.teleporter.sprite.top) {
                        Playnewton.APU.PlaySound("sounds/teleport.wav");
                        this.sara.TeleportOut();
                        Playnewton.GPU.SetSpriteAnimation(this.teleporter.sprite, this.teleporter.teleportAnimation, Playnewton.GPU_AnimationMode.ONCE);
                        this.state = CityIntroLevelState.TELEPORT_SARA;
                    }
                break;
            case CityIntroLevelState.TELEPORT_SARA:
                if(this.teleporter.sprite.animationStopped) {
                    this.fadeoutToNextScene();
                    this.state = CityIntroLevelState.END;
                }
                break;
            case CityIntroLevelState.END:
                break;
            case CityIntroLevelState.STOPPED:
                return;
        }
        if (pad.TestStartAndResetIfPressed()) {
            Playnewton.APU.PlaySound("sounds/skip.wav");
            this.dialog.Skip();
            this.fadeoutToNextScene();
            this.state = CityIntroLevelState.END;
        }
        if (this.sara) {
            this.sara.UpdateBody();
        }
    }

    fadeoutToNextScene() {
        if (!this.fadeout) {
            let layers = [];
            for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
                layers.push(i);
            }
            this.fadeout = new Fadeout(1000, layers, () => {
                this.state = CityIntroLevelState.STOPPED;
                this.Stop();
                this.nextScene = this.nextSceneOnExit;
                this.nextSceneOnExit.Start();
            });
        }
    }

    UpdateSprites() {
        if (this.state === CityIntroLevelState.STOPPED) {
            return;
        }
        this.sara.UpdateSprite();

        if (this.levelAnnouncement) {
            this.levelAnnouncement.Update();
        }
        if (this.fadeout) {
            this.fadeout.Update();
        }
    }
}
