import Scene from "./scene.js"
import * as Playnewton from "../playnewton.js"
import Enemy from "../entities/enemy.js"
import SaraPlane from "../entities/sara_plane.js"
import Z_ORDER from "../utils/z_order.js"
import Fadeout from "../entities/fadeout.js"
import Bald from "../entities/bald.js"
import MovingCloud from "../entities/moving_cloud.js"
import { IngameShmupMapKeyboardEventToPadButton } from "../utils/keyboard_mappings.js"
import Duck from "../entities/duck.js"
import Tengu from "../entities/tengu.js"
import Dragon from "../entities/dragon.js"
import { Dialog } from "../entities/dialog.js"
import Announcement from "../entities/announcement.js"

class PingAnimations {
    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    talk;

    /**
     * @type Playnewton.GPU_SpriteAnimation
     */
    listen;
}

class Ping {

    /**
     * @type PingAnimations
     */
    static animations;

    /**
     * @type Playnewton.GPU_Sprite
     */
    sprite;

    constructor() {
        this.sprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Ping.animations.listen);
        Playnewton.GPU.SetSpriteZ(this.sprite, Z_ORDER.PORTRAIT);
        Playnewton.GPU.SetSpritePosition(this.sprite, Playnewton.GPU.screenWidth - 40, Playnewton.GPU.screenHeight - 128);
    }

    static async Load() {
        let bitmap = await Playnewton.DRIVE.LoadBitmap("sprites/seaplane.png");

        let spriteset = Playnewton.GPU.CreateSpriteset(bitmap, [
            { name: "talk0", x: 1, y: 464, w: 38, h: 38 },
            { name: "talk1", x: 40, y: 464, w: 38, h: 38 },
            { name: "talk2", x: 79, y: 464, w: 38, h: 38 },
            { name: "talk3", x: 118, y: 464, w: 38, h: 38 },
            { name: "listen0", x: 157, y: 464, w: 38, h: 38 },
            { name: "listen1", x: 196, y: 464, w: 38, h: 38 }
        ]);

        Ping.animations = new PingAnimations();

        Ping.animations.talk = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "talk0", delay: 150 },
            { name: "talk1", delay: 150 },
            { name: "talk2", delay: 150 },
            { name: "talk3", delay: 150 },
        ]);

        Ping.animations.listen = Playnewton.GPU.CreateAnimation(spriteset, [
            { name: "listen0", delay: 1000 },
            { name: "listen1", delay: 150 },
            { name: "listen0", delay: 150 },
            { name: "listen1", delay: 150 },
        ]);
    }

    Talk() {
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Ping.animations.talk);
    }

    Listen() {
        Playnewton.GPU.SetSpriteAnimation(this.sprite, Ping.animations.listen);
    }

    static Unload() {
        Ping.animations = null;
    }

    Enable() {
        Playnewton.GPU.EnableSprite(this.sprite);
    }

    Disable() {
        Playnewton.GPU.DisableSprite(this.sprite);
    }
}

/**
 * Enum for level state
 * @readonly
 * @enum {number}
 */
const CloudLevelState = {
    START: 0,
    INTRO_DIALOG: 1,
    ENEMIES_WAVE1: 2,
    ENEMIES_WAVE2: 3,
    ENEMIES_WAVE3: 4,
    ENEMIES_WAVE4: 5,
    ENEMIES_WAVE5: 6,
    ENEMIES_WAVE6: 7,
    ENEMIES_WAVE7: 8,
    ENEMIES_WAVE8: 9,
    BOSS_INTRO: 11,
    BOSS: 12,
    OUTRO_DIALOG: 13,
    GAME_OVER: 14,
    STOPPED: 15
};

export default class CloudLevel extends Scene {

    /**
     * @type CloudLevelState
     */
    state;

    /**
     * @type SaraPlane
     */
    sara;

    /**
     * @type Playnewton.GPU_Bar
     */
    saraHealthBar;

    /**
     * @type Playnewton.GPU_Bar
     */
    dragonHealthBar;

    /**
     * @type Scene
     */
    nextSceneOnExit;

    /**
     * @type Fadeout
     */
    fadeout;

    /**
     * @type ImageBitmap
     */
    backgroundBitmap;

    /**
     * @type Array<MovingCloud>
     */
    movingClouds = [];

    /**
     * @type Playnewton.GPU_Sprite
     */
    backgroundLeftSprite;

    /**
     * @type Playnewton.GPU_Sprite
     */
    backgroundRightSprite;

    /**
     * @type number
     */
    backgroundScrollXSpeed = 1;

    /**
     * @type Array<Enemy>
     */
    enemies = [];

    /**
     * @type Dragon
     */
    dragon;

    /**
     * @type Playnewton.GPU_Label
     */
    dialogLabel;

    /**
     * @type Playnewton.GPU_Label
     */
    skipLabel;

    /**
     * @type Dialog
     */
    dialog;

    /**
     * @type Ping
     */
    ping;

    /**
     * @type Announcement
     */
    bossAnnouncement;

    constructor(nextSceneOnExit) {
        super();
        this.nextSceneOnExit = nextSceneOnExit;
    }

    async InitWorld() {
        for (let z = Z_ORDER.MIN; z <= Z_ORDER.MAX; ++z) {
            let layer = Playnewton.GPU.GetLayer(z);
            Playnewton.GPU.EnableLayer(layer);
        }

        Playnewton.PPU.SetWorldBounds(0, 0, 1024, 576);
        Playnewton.PPU.SetWorldGravity(0, 0);

        this.backgroundBitmap = await Playnewton.DRIVE.LoadBitmap("maps/cloud/cloud_bg.png");
        let backgroundPicture = Playnewton.GPU.CreatePicture(this.backgroundBitmap);

        this.backgroundLeftSprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePicture(this.backgroundLeftSprite, backgroundPicture);
        Playnewton.GPU.SetSpritePosition(this.backgroundLeftSprite, 0, 0);
        Playnewton.GPU.EnableSprite(this.backgroundLeftSprite);

        this.backgroundRightSprite = Playnewton.GPU.CreateSprite();
        Playnewton.GPU.SetSpritePicture(this.backgroundRightSprite, backgroundPicture);
        Playnewton.GPU.SetSpritePosition(this.backgroundRightSprite, this.backgroundLeftSprite.right, 0);
        Playnewton.GPU.EnableSprite(this.backgroundRightSprite);
    }

    InitHUD() {
        this.saraHealthBar = Playnewton.GPU.HUD.CreateBar();
        Playnewton.GPU.HUD.SetBarPosition(this.saraHealthBar, 10, 10);
        Playnewton.GPU.HUD.SetBarSize(this.saraHealthBar, this.sara.maxHealth);
        Playnewton.GPU.HUD.SetBarLevel(this.saraHealthBar, this.sara.health);
        Playnewton.GPU.HUD.EnableBar(this.saraHealthBar);

        this.dragonHealthBar = Playnewton.GPU.HUD.CreateBar();
        Playnewton.GPU.HUD.SetBarPosition(this.dragonHealthBar, 800, 10);
        Playnewton.GPU.HUD.SetBarSize(this.dragonHealthBar, Dragon.MAX_HEALTH);
        Playnewton.GPU.HUD.SetBarLevel(this.dragonHealthBar, Dragon.MAX_HEALTH);
        Playnewton.GPU.HUD.SetBarStyle(this.dragonHealthBar, 8, 16, 4, "#101820", "#9898c8", "#b8006f");

        Playnewton.GPU.EnableHUD(true);
    }

    async InitDialog() {
        this.dialog = new Dialog();

        this.dialogLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.dialogLabel, "bold 32px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.dialogLabel, "left");
        Playnewton.GPU.HUD.SetLabelPosition(this.dialogLabel, 32, Playnewton.GPU.screenHeight - 44);
        Playnewton.GPU.HUD.SetLabelText(this.dialogLabel, "");
        Playnewton.GPU.HUD.SetLabelBackground(this.dialogLabel, 0, Playnewton.GPU.screenHeight - 88, Playnewton.GPU.screenWidth, 88, `#000000cc`);
        Playnewton.GPU.HUD.EnableLabel(this.dialogLabel);

        this.skipLabel = Playnewton.GPU.HUD.CreateLabel();
        Playnewton.GPU.HUD.SetLabelFont(this.skipLabel, "bold 12px monospace");
        Playnewton.GPU.HUD.SetLabelAlign(this.skipLabel, "right");
        Playnewton.GPU.HUD.SetLabelPosition(this.skipLabel, 1024, 564);
        Playnewton.GPU.HUD.SetLabelColor(this.skipLabel, "#eeeeee");
        Playnewton.GPU.HUD.SetLabelText(this.skipLabel, "Skip with ⌨️enter or 🎮start");
        Playnewton.GPU.HUD.EnableLabel(this.skipLabel);
    }

    async Start() {
        this.state = CloudLevelState.START;

        this.fadeout = null;

        await super.Start();

        Playnewton.CTRL.MapKeyboardEventToPadButton = IngameShmupMapKeyboardEventToPadButton;

        let audioPromise = Promise.allSettled([
            Playnewton.APU.PlayMusic("musics/centurion_of_war/overdrive.ogg"),
            Playnewton.APU.Preload("musics/centurion_of_war/helldrake.ogg"),
            Playnewton.APU.Preload("musics/centurion_of_war/chippy_melodyv2.ogg"),
            Playnewton.APU.Preload("sounds/explosion.wav"),
            Playnewton.APU.Preload("sounds/hurt-enemy.wav"),
            Playnewton.APU.Preload("sounds/hurt-sara.wav"),
            Playnewton.APU.Preload("sounds/shoot-bald.wav"),
            Playnewton.APU.Preload("sounds/shoot-dragon.wav"),
            Playnewton.APU.Preload("sounds/shoot-duck.wav"),
            Playnewton.APU.Preload("sounds/shoot-sara.wav"),
            Playnewton.APU.Preload("sounds/shoot-tengu.wav")
        ]);
        this.progress = 0;

        await this.InitWorld();
        this.progress = 10;

        await this.InitDialog();
        this.progress = 20;

        await Ping.Load();
        this.ping = new Ping();
        this.progress = 30;

        await SaraPlane.Load();
        this.sara = new SaraPlane(512, 288);
        this.progress = 40;

        await MovingCloud.Load();
        for (let c = 0; c < 10; ++c) {
            this.movingClouds.push(new MovingCloud());
        }
        this.progress = 50;

        await Tengu.Load();
        this.progress = 60;

        await Duck.Load();
        this.progress = 70;

        await Bald.Load();
        this.progress = 80;

        await Dragon.Load();
        this.progress = 90;

        this.InitHUD();
        this.progress = 95;

        await audioPromise;
        this.progress = 100;
    }

    Stop() {
        super.Stop();

        SaraPlane.Unload();
        this.sara = null;

        MovingCloud.Unload();
        this.movingClouds = [];

        this.backgroundBitmap = null;
        this.backgroundLeftSprite = null;
        this.backgroundRightSprite = null;

        Ping.Unload();
        this.ping = null;

        Tengu.Unload();
        Duck.Unload();
        Bald.Unload();
        Dragon.Unload();
        this.dragon = null;
        this.enemies = []

        this.bossAnnouncement = null;
    }

    UpdatePing() {
        let currentLine = this.dialog.currentLine;
        if(currentLine && currentLine.text.startsWith("[Ping]")) {
            this.ping.Talk();
        } else {
            this.ping.Listen();
        }
    }

    UpdateBodies() {
        let pad = Playnewton.CTRL.GetMasterPad();

        switch (this.state) {
            case CloudLevelState.START:
                this.pausable = false;
                this.dialog.Start([
                    { color: "#e0befb", text: "[Ping] Hi Sara, ready to fight some monsters?" },
                    { color: "#8fffff", text: "[Sara] What? You sold me a touristic flight !" },
                    { color: "#e0befb", text: "[Ping] We send tourists to kill monster." },
                    { color: "#e0befb", text: "[Ping] Thank for helping our democratic republic." },
                    { color: "#8fffff", text: "[Sara] I’ll get you back tonight Ping..." }
                ]);
                this.state = CloudLevelState.INTRO_DIALOG;
                this.ping.Enable();
                break;
            case CloudLevelState.INTRO_DIALOG:
                this.dialog.Update(this.dialogLabel);
                if (pad.TestStartAndResetIfPressed()) {
                    Playnewton.APU.PlaySound("sounds/skip.wav");
                    this.dialog.Skip();
                }
                if (this.dialog.done) {
                    this.ping.Disable();
                    Playnewton.GPU.HUD.DisableLabel(this.dialogLabel);
                    Playnewton.GPU.HUD.DisableLabel(this.skipLabel);
                    this.enemies.push(new Duck(Playnewton.PPU.world.bounds.right, 288));
                    this.state = CloudLevelState.ENEMIES_WAVE1;
                } else {
                    this.UpdatePing();
                }
                break;
            case CloudLevelState.ENEMIES_WAVE1:
                this.pausable = true;
                this.handleSaraDeath();
                if (this.enemies.every((e) => e.dead)) {
                    this.enemies.push(new Duck(Playnewton.PPU.world.bounds.right, 100));
                    this.enemies.push(new Duck(Playnewton.PPU.world.bounds.right, 400));
                    this.state = CloudLevelState.ENEMIES_WAVE2;
                }
                break;
            case CloudLevelState.ENEMIES_WAVE2:
                this.handleSaraDeath();
                if (this.enemies.every((e) => e.dead)) {
                    this.enemies.push(new Tengu(Playnewton.PPU.world.bounds.right, 288, Playnewton.PPU.world.bounds.right - 100, 150));
                    this.state = CloudLevelState.ENEMIES_WAVE3;
                }
                break;
            case CloudLevelState.ENEMIES_WAVE3:
                this.handleSaraDeath();
                if (this.enemies.every((e) => e.dead)) {
                    this.enemies.push(new Tengu(Playnewton.PPU.world.bounds.right, 244, Playnewton.PPU.world.bounds.right - 100, 100));
                    this.enemies.push(new Tengu(Playnewton.PPU.world.bounds.right, 288, Playnewton.PPU.world.bounds.right - 100, 200));
                    this.state = CloudLevelState.ENEMIES_WAVE4;
                }
                break;
            case CloudLevelState.ENEMIES_WAVE4:
                this.handleSaraDeath();
                if (this.enemies.every((e) => e.dead)) {
                    this.enemies.push(new Bald(Playnewton.PPU.world.bounds.right, 288, Playnewton.PPU.world.bounds.right - 100, 288));
                    this.state = CloudLevelState.ENEMIES_WAVE5;
                }
                break;
            case CloudLevelState.ENEMIES_WAVE5:
                this.handleSaraDeath();
                if (this.enemies.every((e) => e.dead)) {
                    this.enemies.push(new Bald(Playnewton.PPU.world.bounds.right, 100, Playnewton.PPU.world.bounds.right - 100, 288));
                    this.enemies.push(new Duck(Playnewton.PPU.world.bounds.right, 100));
                    this.enemies.push(new Duck(Playnewton.PPU.world.bounds.right, 200));
                    this.state = CloudLevelState.ENEMIES_WAVE6;
                }
                break;
            case CloudLevelState.ENEMIES_WAVE6:
                this.handleSaraDeath();
                if (this.enemies.every((e) => e.dead)) {
                    this.enemies.push(new Tengu(Playnewton.PPU.world.bounds.right, 288, Playnewton.PPU.world.bounds.right - 100, 100));
                    this.enemies.push(new Tengu(Playnewton.PPU.world.bounds.right, 244, Playnewton.PPU.world.bounds.right - 100, 200));
                    this.enemies.push(new Duck(Playnewton.PPU.world.bounds.right, 200));
                    this.enemies.push(new Duck(Playnewton.PPU.world.bounds.right, 400));
                    this.state = CloudLevelState.ENEMIES_WAVE7;
                }
                break;
            case CloudLevelState.ENEMIES_WAVE7:
                this.handleSaraDeath();
                if (this.enemies.every((e) => e.dead)) {
                    this.enemies.push(new Bald(Playnewton.PPU.world.bounds.right, 100, Playnewton.PPU.world.bounds.right - 100, 200));
                    this.enemies.push(new Bald(Playnewton.PPU.world.bounds.right, 100, Playnewton.PPU.world.bounds.right - 100, 400));
                    this.state = CloudLevelState.ENEMIES_WAVE8;
                }
                break;
            case CloudLevelState.ENEMIES_WAVE8:
                this.handleSaraDeath();
                if (this.enemies.every((e) => e.dead)) {
                    Playnewton.APU.PlayMusic("musics/centurion_of_war/helldrake.ogg");
                    this.bossAnnouncement = new Announcement("Enter the dragon");
                    this.bossAnnouncement.Start();
                    this.state = CloudLevelState.BOSS_INTRO;
                }
                break;
            case CloudLevelState.BOSS_INTRO:
                if(this.bossAnnouncement.done) {
                    this.dragon = new Dragon(Playnewton.PPU.world.bounds.right, 288);
                    this.enemies.push(this.dragon);
                    this.state = CloudLevelState.BOSS;
                    Playnewton.GPU.HUD.EnableBar(this.dragonHealthBar);
                }
                break;
            case CloudLevelState.BOSS:
                this.handleSaraDeath();
                if (this.enemies.every((e) => e.dead)) {
                    this.dialog.Start([
                        { color: "#e0befb", text: "[Ping] Bravo Sara you killed them all !" },
                        { color: "#8fffff", text: "[Sara] I'm flying back to the hotel." },
                        { color: "#8fffff", text: "[Sara] Get ready for my revenge !" },
                        { color: "#e0befb", text: "[Ping] I will look forward to it..." },
                        { color: "#ffffff", text: "THE END", speed: 200, align: "center", x: 512, y: 532, delay: 10000 }
                    ]);
                    this.state = CloudLevelState.OUTRO_DIALOG;
                    this.ping.Enable();
                    Playnewton.GPU.HUD.SetLabelText(this.dialogLabel, "");
                    Playnewton.GPU.HUD.EnableLabel(this.dialogLabel);
                    Playnewton.GPU.HUD.EnableLabel(this.skipLabel);
                }
                break;
            case CloudLevelState.OUTRO_DIALOG:
                this.pausable = false;
                this.handleSaraDeath();
                this.dialog.Update(this.dialogLabel);
                this.UpdatePing();
                if (pad.TestStartAndResetIfPressed()) {
                    Playnewton.APU.PlaySound("sounds/skip.wav");
                    this.fadeoutToNextScene();
                }
                break;
            case CloudLevelState.GAME_OVER:
                this.pausable = false;
                if (pad.TestStartAndResetIfPressed()) {
                    Playnewton.APU.PlaySound("sounds/pause_resume.wav");
                    this.state = CloudLevelState.STOPPED;
                    this.Stop();
                    this.nextScene = this.nextSceneOnExit;
                    this.nextSceneOnExit.Start();
                }
                return;
            case CloudLevelState.STOPPED:
                return;
        }
        this.sara.UpdateBody();

        for (let cloud of this.movingClouds) {
            cloud.Update();
        }

        for (let enemy of this.enemies) {
            enemy.UpdateBody();
        }
    }

    handleSaraDeath() {
        if (this.sara.dead) {
            let layers = [];
            for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
                if (i !== Z_ORDER.SARA && i !== Z_ORDER.EXPLOSION) {
                    layers.push(i);
                }
            }
            this.fadeout = new Fadeout(1000, layers, () => {
                Playnewton.APU.PlayMusic("musics/centurion_of_war/chippy_melodyv2.ogg");

                let gameoverLabel = Playnewton.GPU.HUD.CreateLabel();
                Playnewton.GPU.HUD.SetLabelFont(gameoverLabel, "bold 48px monospace");
                Playnewton.GPU.HUD.SetLabelColor(gameoverLabel, "#ff0000");
                Playnewton.GPU.HUD.SetLabelAlign(gameoverLabel, "center");
                Playnewton.GPU.HUD.SetLabelPosition(gameoverLabel, 512, 288);
                Playnewton.GPU.HUD.SetLabelText(gameoverLabel, "Game over");
                Playnewton.GPU.HUD.EnableLabel(gameoverLabel);
                Playnewton.GPU.HUD.DisableBar(this.saraHealthBar);

                let continueLabel = Playnewton.GPU.HUD.CreateLabel();
                Playnewton.GPU.HUD.SetLabelFont(continueLabel, "bold 12px monospace");
                Playnewton.GPU.HUD.SetLabelAlign(continueLabel, "right");
                Playnewton.GPU.HUD.SetLabelPosition(continueLabel, 1024, 564);
                Playnewton.GPU.HUD.SetLabelColor(continueLabel, "#eeeeee");
                Playnewton.GPU.HUD.SetLabelText(continueLabel, "Press ⌨️enter or 🎮start");
                Playnewton.GPU.HUD.EnableLabel(continueLabel);
            });
            this.state = CloudLevelState.GAME_OVER;
        }
    }

    fadeoutToNextScene() {
        if (!this.fadeout) {
            let layers = [];
            for (let i = Z_ORDER.MIN; i <= Z_ORDER.MAX; ++i) {
                layers.push(i);
            }
            this.fadeout = new Fadeout(1000, layers, () => {
                this.Stop();
                this.nextScene = this.nextSceneOnExit;
                this.nextSceneOnExit.Start();
            });
        }
    }

    UpdateSprites() {
        if(this.state === CloudLevelState.STOPPED) {
            return;
        }
        let backgroundLeftSpriteX = this.backgroundLeftSprite.x - this.backgroundScrollXSpeed;
        if (backgroundLeftSpriteX < -this.backgroundLeftSprite.width) {
            backgroundLeftSpriteX = 0;
        }
        Playnewton.GPU.SetSpritePosition(this.backgroundLeftSprite, backgroundLeftSpriteX, 0);
        Playnewton.GPU.SetSpritePosition(this.backgroundRightSprite, this.backgroundLeftSprite.right, 0);

        this.sara.UpdateSprite();
        Playnewton.GPU.HUD.SetBarLevel(this.saraHealthBar, this.sara.health);
        
        if(this.dragon) {
            Playnewton.GPU.HUD.SetBarLevel(this.dragonHealthBar, this.dragon.health);
        }        

        if (this.fadeout) {
            this.fadeout.Update();
        }

        for (let enemy of this.enemies) {
            enemy.UpdateSprite();
            enemy.Pursue(this.sara);
            this.sara.Pursue(enemy);
        }

        if(this.bossAnnouncement) {
            this.bossAnnouncement.Update();
        }
    }
}
