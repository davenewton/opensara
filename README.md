MOVED TO https://codeberg.org/devnewton/opensara

# opensara

Opensara is an opensource Platformer/shmup game starring [opengameart.org](https://opengameart.org/) mascot : Sara.

![Sara](maps/icons/sara-icon.png)

![Gameplay](docs/gameplay.webp)

## How to play ?

The latest stable version is playable on [play.devnewton.fr](https://play.devnewton.fr).

To run the game locally, use a local webserver like [Caddy](https://caddyserver.com/) :
caddy file-server --listen :2023
    caddy file-server --listen :2023

## Licenses

Unless stated otherwise, everything is under MIT license.

Some assets are under CC0 or CC-BY license as noted in LICENSE.md files in project folders.

## Acknowledgements

Thanks to [Libresprite](http://www.libresprite.org/) developers. I use it to draw my sprites.

![Libresprite](docs/libresprite.png)

Thanks to [jsfxr](https://sfxr.me/) authors, I use it for sound effects.

![jsfxr](docs/jsfxr.png)

Thanks to [Tiled](https://www.mapeditor.org/) developers. I use it for level edit.

![tiled](docs/tiled.png)

Thanks to [Centurion_of_war](https://opengameart.org/users/centurionofwar) for his great musics.